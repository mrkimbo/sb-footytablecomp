/**
 * Created with JetBrains PhpStorm.
 * User: kimh
 * Date: 13/09/13
 * Time: 11:23 AM
 */

(function(){

  'use strict';

  /**
   * QuerySelector helpers that automatically return results as iterable arrays
   */
  Element.prototype.find = document.find = function() {
    return this.querySelector.apply(this, arguments);
  };
  Element.prototype.findAll = document.findAll = function() {
    var r = this.querySelectorAll.apply(this, arguments);
    return r.length ? Array.prototype.constructor.apply(null,r) : [];
  };

  /**
   * JS Class inheritance:
   * @param {Function} superClass
   */
  Function.prototype.extends = function(SuperClass) {
    // copy basic prototype methods from SuperClass into this:
    this.prototype = new SuperClass();
    this.constructor = this;
    this.prototype._super = new SuperClass();

    return this;
  };

  var Utils = {

    /**
     * MouseEnter/MouseLeave functionality.
     * @param {HTMLElement} el
     * @param {Function} overFn
     * @param {Function} outFn
     */
    hover: function(el, overFn, outFn) {
      var over = false;
      if(/Array/.test(Object.prototype.toString.call(el))) {
        el = el[0];
      }

      var _ovr = function(e) {
        if(e.target === el || el.contains(e.target)) {
          if(over) return;
          over = true;
          //log('over');
          overFn.apply(this, arguments);
        }
        else
        {
          if(!over) return;
          over = false;
          //log('out1');
          outFn.apply(this, arguments);
        }
      };
      var _out = function(e) {
        if(over && !e.relatedTarget) {
          over = false;
          //log('out2: ' + e.type);
          outFn.apply(this, arguments);
        }
      };

      document.addEventListener('mouseover', _ovr);
      // trap for browser/frame mouseout (relatedTarget == null) //
      document.addEventListener('mouseout', _out);

      // If on touch device, pause animations if user gets all touchy //
      if(this.isTouchEnabled()) {
        document.addEventListener('touchstart', _ovr);
      }
    },

    /**
     * Create delegate function for maintaining scope in callbacks.
     * @param {Function} fn Function to execute.
     * @param {Object} selfObj Scope in which to execute the callback.
     * @param {...*} var_args Arguments to pass to final callback.
     * @returns {Function}
     */
    bind: function(fn, selfObj) {
      if (arguments.length > 2) {
        var boundArgs = Array.prototype.slice.call(arguments, 2);
        return function() {
          // Prepend the bound arguments to the current arguments.
          var newArgs = Array.prototype.slice.call(arguments);
          Array.prototype.unshift.apply(newArgs, boundArgs);
          return fn.apply(selfObj, newArgs);
        };

      } else {
        return function() {
          return fn.apply(selfObj, arguments);
        };
      }
    },

    /**
     * From underscore library...
     * "The cornerstone, an 'each' implementation, aka 'forEach'.
     *  Handles objects with the built-in 'forEach', arrays, and raw objects.
     *  Delegates to **ECMAScript 5**'s native `forEach` if available."
     *
     * @param {object} obj Object or Array to traverse.
     * @param {Function} iterator Function to execute
     * @param {object} context Scope in which to execute iterator function.
     */
    each: function(obj, iterator, context) {
      if(obj === null) return;
      if (Array.forEach && obj.forEach === Array.forEach) {
        obj.forEach(iterator, context);
      } else if (obj.length === +obj.length) {
        for (var i = 0, l = obj.length; i < l; i++) {
          if (iterator.call(context, obj[i], i, obj) === {}) {
            return;
          }
        }
      } else {
        for (var key in obj) {
          if (obj.hasOwnProperty(key)) {
            if (iterator.call(context, obj[key], key, obj) === {}){
              return;
            }
          }
        }
      }
    },

    /**
     * Forces array type to given parameter.
     * - if array passed, returns param.
     * - if non-array passed, returns new array containing param at index 0.
     * - if nothing passed, returns an empty array.
     * @param {*} a
     * @returns {Array}
     */
    toArray: function(a) {
      return (/array/i).test(Object.prototype.toString.call(a)) ? a :
        typeof a === 'undefined' ? [] : [a];
    },

    /**
     * Check if we're in a mobile browser.
     * @returns {boolean}
     */
    isMobile: function() {
      var b = false;
      (function(a) {
        b = (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) ||
          /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)));
      })(navigator.userAgent||navigator.vendor||window.opera);
      return b;
    },

    /**
     * Reports whether touch interaction is available.
     * @returns {boolean}
     */
    isTouchEnabled: function() {
      return !!('ontouchstart' in window);
    },

    /**
     *Check if we're in Internet Explorer
     */
    isIE: function() {
      return (/MSIE/i).test(navigator.userAgent);
    },

    /**
     * Checks if value is defined.
     * @param {*} val
     * @returns {boolean}
     */
    isDefined: function(v) {
      return typeof(v) !== 'undefined' && v !== null;
    },

    isArray: function(v) {
      return (/array/i).test(Object.prototype.toString.call(v));
    },
    isObject: function(v) {
      return (/object/i).test(Object.prototype.toString.call(v));
    },

    /**
     * Checks if supplied property is empty:
     * @param {*} v Property to check
     */
    isEmpty: function(v) {
      if(this.isArray(v)) {
        return v.length === 0;
      }
      if(this.isObject(v)) {
        for(var i in v) break;
        return !this.isDefined(i);
      }

      // test property has a value:
      return !this.isDefined(v);
    },

    /**
     * Replacement for GSAP CSSPlugin as it's 32k.
     * Applies 'px' based CSS values to tween targets.
     * Note: Only supports px,% and deg units. Initial values of 'auto' are
     * treated as 'px'
     */
    applyCSS: function() {
      var i, v, s, props = this._propLookup;
      for(i in props) {
        if(props.hasOwnProperty(i)) {
          // if first time accessed, store initial value and eval units:
          if(props[i].start === undefined) {
            s = getComputedStyle(this._propLookup[i].t)[i];
            if(s === 'auto') {
              props[i].unit = 'px';
            } else {
              props[i].unit = s.match(/[^0-9]+$/i) || '';
            }
            props[i].start = parseFloat(s) || 0.0;
          }
          v = props[i].start + ((this.vars[i] - props[i].start) * this.ratio);
          /*log(props[i].start + '[' + props[i].unit + '] -> ' + this.vars[i] +
           '*' + this.ratio + ': ' + v
           );*/
          this.target.style[i] = v + props[i].unit;
        }
      }
    },

    /**
     * Browser safe className toggle function.
     * @param el
     * @param cls
     * @param bOn
     */
    toggleClass: function(el, cls, bOn) {
      if(!el) return;
      var a = el.className.split(/\s/).filter(function(item) {
        return item !== '' && item.toLowerCase() !== cls.toLowerCase();
      });
      if(bOn) a.push(cls);
      el.className = a.join(' ');
    }
  };

  /**
   * Logging helper.
   * @param {...*} args
   */
  window.log = function() {
    if(!window.console || !window.console.log || window.DEBUG === false) {
      return;
    }
    // ie9 fix:
    if(typeof console.log === 'object') {
      Function.prototype.bind.call(console.log,console)
        .apply(console,arguments);
    } else {
      console.log.apply(console,arguments);
    }
  };

  // drop into window scope:
  window.utils = Utils;

})();
