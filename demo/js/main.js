/**
 * Created with WebStorm.
 * User: kimh
 * Date: 10/07/2014
 * Time: 15:43
 */

/* global $, FB, FBModel, utils, log */

(function() {

  'use strict';

  var FBApp = function(devMode) {

    // props:
    this._devMode = devMode;
    this._model = new FBModel(
      devMode ? ID.test : ID.live
    );

    // buttons:
    this._loginBtn = null;
    this._logoutBtn = null;
    this._revokeBtn = null;
    this._likeBtn = null;
    this._submitBtn = null;

    // info fields:
    this._userField = null;
    this._statusField = null;
    this._canPostField = null;
    this._likeField = null;
    this._readyField = null;
    this._returningField = null;
    this._friendList = null;

    this._friendTemplate = null;
  };

  FBApp.prototype = {

    init: function() {
      var options = {
        version:  'v2.0',
        appId:    this._model.appId,
        cookie:   true,
        status:   true,
        xfbml:    true,
        channel:  'http://www.footyshowtable.com/php/channel.php'
      };
      FB.init(options);

      this._getElements();
      this._addUIListeners();
      this._addFBListeners();
      this._getLoginStatus();
    },

    _getElements: function() {
      // buttons:
      this._loginBtn = $('#login-btn');
      this._logoutBtn = $('#logout-btn');
      this._revokeBtn = $('#revoke-btn');
      this._likeBtn = $('#like-btn');
      this._submitBtn = $('#submit-btn');

      // info fields:
      this._userField = $('#user span:last-child');
      this._statusField = $('#status span:last-child');
      this._canPostField = $('#can-post span:last-child');
      this._likeField = $('#likes-sportsbet span:last-child');
      this._returningField = $('#returning-user span:last-child');
      this._readyField = $('#ready span:last-child');

      // friend list:
      this._friendList = $('#friend-list');

      // friend template (:
      this._friendTemplate = $('#friend-template').html().trim();
    },

    _addUIListeners: function() {
      this._loginBtn.on('click', utils.bind(this.login, this));
      this._logoutBtn.on('click', utils.bind(this.logout, this));
      this._revokeBtn.on('click', utils.bind(this.revokePerms, this));
      this._submitBtn.on('click', utils.bind(this.enterCompetition, this));

      // Model update listener:
      $(document).on(
        FBModel.Event.UPDATE,
        utils.bind(this._onModelUpdate, this)
      );
    },

    _addFBListeners: function() {
      FB.Event.subscribe('auth.logout', function(response) {
        window.location.reload();
      });
      FB.Event.subscribe('edge.create', utils.bind(function(href, widget) {
        if(href.indexOf('sportsbetcomau') === -1) return;
        this._model.hasLike = true;
        this._updateButtons();
      }, this));
      FB.Event.subscribe('edge.remove', utils.bind(function(href, widget) {
        if(href.indexOf('sportsbetcomau') === -1) return;
        this._model.hasLike = false;
        this._updateButtons();
      }, this));
    },

    login: function() {
      FB.login(
        utils.bind(this._onStatusChange, this), {
          scope: FBModel.basicPermissions
        }
      );
    },

    logout: function() {
      FB.logout(null);
    },

    enterCompetition: function(requestType, response) {
      if(requestType === 'check-sb-like') {
        this._model.hasLike = response.data.length >= 1;
        if(!this._model.hasLike) {
          log('!! MUST LIKE PAGE !!');
          return;
        }
      }
      if(!this._model.hasLike) {
        this._checkSBLike(this.enterCompetition);
        return;
      }

      // ** DEV VALUES **
      if(!this._model.isReturningUser) {
        log(this + '::enterCompetition() - !! USING TEST DATA !!');
        this._model._entry = TEST_ENTRY;
        this._model._selections = [0,1,2,3,4,5,6,7,8,9];
      }

      // Push to server:
      log(this + '::enterCompetition()');
      this._model.save();
    },

    revokePerms: function() {
      // revoke permissions:
      FB.api(
        '/me/permissions',
        'DELETE',
        this._onAPIResponse.bind(this, 'delete-perms')
      );
    },

    _onStatusChange: function(response) {
      log(this + '::onStatusChange()', response);
      this._statusField.text(response.status);

      switch(response.status.toLowerCase()) {
        case 'connected':
          this._model.state = FBModel.STATE.app_auth;
          this._model.authInfo = response.authResponse;
          this._getBasicInfo();
          this._getTaggableFriends();
          this._checkSBLike();
          this._checkPermissions();
          break;

        case 'not_authorized':
          this._model.state = FBModel.STATE.fb_auth;
          this._loginBtn.text('Login to the App');
          break;

        default:
          this._model.state = FBModel.STATE.unknown;
          this._loginBtn.text('Login to Facebook');
          break;
      }

      this._updateButtons();
    },

    _getLoginStatus: function() {
      log(this + '::getLoginStatus()');
      FB.getLoginStatus(
        utils.bind(this._onStatusChange, this)
      );
    },

    _getBasicInfo: function() {
      FB.api(
        '/me',
        utils.bind(this._onAPIResponse, this, 'basic-info')
      );
    },

    _getTaggableFriends: function() {
      FB.api(
        '/me/taggable_friends',
        utils.bind(this._onAPIResponse, this, 'friends')
      );
    },

    _checkSBLike: function(callback) {
      FB.api(
        '/me/likes/' + ID.sbPage,
        utils.bind((callback || this._onAPIResponse), this, 'check-sb-like')
      );
    },

    _checkPermissions: function() {
      FB.api('/me/permissions',
      utils.bind(this._onAPIResponse, this, 'perms'));
    },

    _onAPIResponse: function(requestType, response) {
      log(this + '::onAPIResponse(' + requestType + ')');
      log(response);

      switch(requestType) {
        case 'basic-info':
          this._model.userInfo = response;
          this._model.checkForEntry();
          this._userField.text(this._model.userInfo.name);
          break;

        case 'friends':
          var item;
          this._model.friends = response.data;
          utils.each(this._model.friends,function(friend) {
            item = document.createElement('li');
            item.innerHTML = this._friendTemplate
              .replace('{IMG}',friend.picture.data.url)
              .replace('{NAME}',friend.name);
            this._friendList.append(item);
          }, this);
          if(response.paging.hasOwnProperty('next')) {
            // ToDo: handle paged friend response

          }
          break;

        case 'perms':
          this._model.canPost = false;
          utils.each(response.data, function(perm) {
            if(perm.permission === 'publish_actions') {
              this._model.canPost = perm.status === 'granted';
            }
          }, this);
          this._canPostField.text(this._model.canPost ? 'Yes' : 'No');
          break;

        case 'check-sb-like':
          this._model.hasLike = response.data.length >= 1;
          this._likeField.text(this._model.hasLike ? 'Yes' : 'No');
          break;

        case 'add-sb-like':

          break;

        case 'remove-sb-like':

          break;

        case 'delete-perms':
          window.location.reload();
          break;
      }

      this._updateView();
    },

    _onModelUpdate: function(evt, type) {
      log(this + '::onModelUpdate(' + type + ')');
      switch(type.toLowerCase()) {
        case 'retrieve':
          this._returningField.text(this._model.isReturningUser ? 'Yes' : 'No');
          if(this._model.isReturningUser) {
            this._submitBtn.text('Update Entry');
          }
          break;
      }

      this._updateView();
    },

    _updateView: function() {
      this._updateButtons();
      this._readyField.text(this._model.isReady() ? 'Yes' : 'No');

      $('.info').toggleClass('hidden', this._model.userInfo === null);
      $('.details').toggleClass('hidden', this._model.friends === null);
    },

    _updateButtons: function() {
      //log(this + '::updateButtons() - isAuth: ' + this._model.isAuthorised());
      this._loginBtn.toggleClass('hidden', this._model.isAuthorised());
      this._logoutBtn.toggleClass('hidden', !this._model.isAuthorised());
      this._revokeBtn.toggleClass('hidden', !this._model.isAuthorised());
      this._submitBtn.toggleClass('hidden', !this._model.isAuthorised());
      this._likeBtn.toggleClass('hidden', !this._model.isAuthorised() ||
        (this._model.isAuthorised() && this._model.hasLike)
      );
    },

    _onUserCancelled: function() {
      log(this + '::onUserCancelled()');
    },

    toString: function() {
      return 'FBApp';
    }

  };

  var ID = {
    test: '604792542973649',
    live: '604289759690594',
    sbPage: '113208555377897'
  };
  var TEST_ENTRY = {
    first_name: 'teste',
    last_name: 'pok',
    phone: '999',
    state: 'VIC',
    email: 'kim.holland@sportsbet.com.au',
  };

  // browser tests:
  // ToDo: Add browser support tests?
  var ok = true;
  if(!ok) {

    // ToDo: Add unsupported browser view
    console.log('Unsupported browser');


  } else {
    var app = window.app = new FBApp(true);
    app.init();
  }

})();
