/**
 * Created with WebStorm.
 * User: kimh
 * Date: 12/07/2014
 * Time: 16:11
 */

/* global $, log, utils */

(function() {

  'use strict';

  var FBModel = function(appId) {

    this.state = FBModel.STATE.not_set;
    this.appId = appId;

    this.userInfo = null;
    this.friends = null;
    this.hasLike = null;
    this.canPost = null;
    this.isReturningUser = null;

    // Form data:
    this._entry = null;
    this._selections = [];

    this.init();
  };

  FBModel.STATE = {
    not_set: -1,
    unknown: 0,
    fb_auth: 1,
    app_auth: 2
  };

  FBModel.Event = {
    UPDATE: 'FBModel:onUpdate',
    SELECTION_ADDED: 'FBModel:onSelectionAdded',
    SELECTION_REMOVED: 'FBModel:onSelectionRemoved'
  };

  FBModel.basicPermissions = 'public_profile,email,user_likes,user_friends';
  FBModel.extendedPermissions = 'publish_actions';

  FBModel.prototype = {

    init: function() {
      $.ajaxSetup(
        {
          async: true,
          type: 'GET',
          dataType: 'json',
          url: 'http://www.footyshowtable.com/php/api.php'
        }
      );
    },

    isAuthorised: function() {
      return this.state === FBModel.STATE.app_auth;
    },

    isReady: function() {
      switch(this.state) {
        case FBModel.STATE.app_auth:
          return this.userInfo !== null &&
            this.isReturningUser !== null &&
            this.friends !== null &&
            this.canPost !== null &&
            this.hasLike !== null;

        case FBModel.STATE.fb_auth:
        case FBModel.STATE.unknown:
          // has state - nothing else needed:
          return true;

        default:
          // no state - not ready:
          return false;
      }
    },

    save: function() {
      var cmd = this.isReturningUser ? 'update' : 'insert';

      this._entry.phone = this._entry.phone.replace(/[^0-9]/g,'');
      var data = $.extend(this._entry, {
        fb_id: this.userInfo.id,
        entry: this._selections.join(',')
      });
      log(data);
      this._sendCommand(cmd, data);
    },

    addSelection: function(id) {
      this._selections.push(id);
      $.event.trigger(FBModel.Event.SELECTION_ADDED, id);
    },

    removeSelection: function(id) {
      this._selections.splice(this._selections.indexOf(id), 1);
      $.event.trigger(FBModel.Event.SELECTION_REMOVED, id);
    },

    checkForEntry: function() {
      this._sendCommand('retrieve', {
        'fb_id': this.userInfo.id
      });
    },

    _sendCommand: function(cmd, data) {
      log(this + '::sendCommand(' + cmd + ')');
      data.action = cmd;
      $.ajax({
        data: data,
        success: utils.bind(this._onResponse, this, cmd)
      });
    },

    _onResponse: function(responseType, response) {
      switch(responseType) {
        case 'insert':
          break;

        case 'update':
          break;

        case 'retrieve':
          this.isReturningUser = response.success;
          if(!response.success) break;

          // repopulated stored selections:
          utils.each(response.data.entry.split(','), function(id) {
            this.addSelection(id);
          }, this);
          this._entry = response.data;
          delete this._entry.entry;
          break;
      }

      log(
        this + '::onResponse(' + responseType + ') - operation ' +
          (response.success ? 'succeeded' : 'failed')
      );

      // dispatch event to notify listeners of model update:
      $.event.trigger(FBModel.Event.UPDATE, responseType);
    },

    toString: function() {
      return 'FBModel';
    }

  };

  window.FBModel = FBModel;

})();