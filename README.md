SportsBet FootyTable Competition - An early Angular project
=====================================================

##Installation:
Install grunt globally:
```
npm install -g grunt-cli
```

####Install Dependencies
```
$ npm install
```

### Build Project
```
$ grunt
```

### Serve
This project is not viewable locally and requires dedicated installation.

However, please enjoy the source :)