// Generated on 2013-09-23 using generator-angular 0.4.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);

  // configurable paths
  var _config = {
    src: 'src',
    deploy: 'deploy'
  };

  grunt.initConfig(
    {
      config: _config,
      debug: false,

      autoprefixer: {
        options: ['last 1 version'],
        dist: {
          files: [
            {
              expand: true,
              cwd: '.tmp/css/',
              src: '{,*/}*.css',
              dest: '.tmp/css/'
            }
          ]
        }
      },
      clean: {
        dist: {
          files: [
            {
              dot: true,
              src: [
                '.tmp',
                '<%= config.deploy %>/*',
                '!<%= config.deploy %>/.git*'
              ]
            }
          ]
        },
        server: '.tmp'
      },
      jshint: {
        options: {
          jshintrc: '.jshintrc'
        },
        all: [
          'Gruntfile.js',
          '<%= config.src %>/js/{,*/}*.js'
        ]
      },
      useminPrepare: {
        html: '<%= config.src %>/*.{html,php}',
        options: {
          dest: '<%= config.deploy %>'
        }
      },
      usemin: {
        html: ['<%= config.deploy %>/*.{html,php}'],
        css: ['<%= config.deploy %>/css/{,*/}*.css'],
        options: {
          dirs: ['<%= config.deploy %>']
        }
      },
      imagemin: {
        dist: {
          files: [
            {
              expand: true,
              cwd: '<%= config.src %>/img',
              src: '{,*/}*.{png,jpg,jpeg,gif}',
              dest: '<%= config.deploy %>/img'
            }
          ]
        }
      },
      cssmin: {
        // By default, your `index.html` <!-- Usemin Block --> will take care of
        // minification. This option is pre-configured if you do not wish to use
        // Usemin blocks.
        // dist: {
        //   files: {
        //     '<%= config.deploy %>/css/main.css': [
        //       '.tmp/css/{,*/}*.css',
        //       '<%= config.src %>/css/{,*/}*.css'
        //     ]
        //   }
        // }
      },
      htmlmin: {
        dist: {
          options: {
            //removeCommentsFromCDATA: true,
            // https://github.com/config/grunt-usemin/issues/44
            //collapseWhitespace: true,
            //collapseBooleanAttributes: true,
            //removeAttributeQuotes: true,
            //removeRedundantAttributes: true,
            //useShortDoctype: true,
            //removeEmptyAttributes: true,
            //removeOptionalTags: true
          },
          files: [
            {
              expand: true,
              cwd: '<%= config.src %>',
              src: ['*.{html,php}'],
              dest: '<%= config.deploy %>'
            }
          ]
        }
      },
      // Put files not handled in other tasks here
      copy: {
        dist: {
          files: [
            {
              expand: true,
              dot: true,
              cwd: '<%= config.src %>',
              dest: '<%= config.deploy %>',
              src: [
                '*.{ico,png,txt}',
                //'.htaccess',
                'api/**/*'
                //'img/*',
                //'js/third-party/**/*.js'
              ]
            },
            {
              expand: true,
              cwd: '.tmp/img',
              dest: '<%= config.deploy %>',
              src: [
                'generated/*'
              ]
            }
          ]
        },
        styles: {
          expand: true,
          cwd: '<%= config.src %>/css',
          dest: '.tmp/css/',
          src: '{,*/}*.css'
        },
        templates: {
          expand: true,
          cwd: '<%= config.src %>/html',
          dest: '<%= config.deploy %>/html',
          src: '*'
        }
      },
      concurrent: {
        dist: [
          'copy:styles',
          'imagemin',
          'htmlmin'
        ]
      },
      ngmin: {
        dist: {
          files: [
            {
              expand: true,
              cwd: '<%= config.deploy %>/js',
              src: '*.js',
              dest: '<%= config.deploy %>/js'
            }
          ]
        }
      },
      uglify: {
        dist: {
          files: {
            '<%= config.deploy %>/js/main.min.js': [
              '<%= config.deploy %>/js/main.min.js'
            ]
          }
        }
      },
      replace: {
        dist: {
          options: [
            {
              // Rename console.log statements to just 'log'.
              find: /([^window\.])console\.\w+?\(/gi,
              repl: '$1log('
            },
            {
              // Update date meta tag.
              find: /(<meta.+?"date".+?content=").*?"/i,
              repl: '$1<%= grunt.template.today("yyyy-mm-dd HH:mm:ss") %>"'
            },
            {
              // Remove comments marked for removal (multiline).
              find: /<!-+~[\s\S]+?-->\n/gi,
              repl: ''
            },
            {
              // Remove items marked for deletion (multiline).
              find: /<.*?(?=data-remove)[\s\S]+?<\/\w+>\n?/gm,
              repl: ''
            },
            {
              // Uncomment items marked for inclusion (multiline).
              find: /<!-+\+([\s\S]+?)-->\n/gi,
              repl: '$1\n'
            },
            {
              // Remove link protocols:
              find: /(=['"])https?:(?=\/\/)/gi,
              repl: '$1'
            }
          ],
          src: [
            '<%= config.deploy %>/{js,css}*.min.{css,js}',
            '<%= config.deploy %>/*.{html,php}',
            '<%= config.deploy %>/html/*.html'
          ]
        },
        debug: {
          options: [
            {
              // Add debug flag after banner in main.min.js //
              find: /^/,
              repl: 'var DEBUG=<%=debug%>;'
            }
          ],
          src: ['<%= config.deploy %>/js/main.min.js']
        },
        meta: {
          options: [
            {
              // re-add protocols to fb open graph meta tags //
              find: /(<meta property.*=['"])(?=\/\/)/ig,
              repl: '$1https:'
            }
          ],
          src: ['<%= config.deploy %>/*.{html,php}']
        }
      }
    }
  );

  grunt.registerMultiTask('replace', '', function() {
    var s;

    grunt.log.writeln('Applying regex replace to following files:');
    this.filesSrc.forEach(function(file) {
      grunt.log.writeln('    - '.white + file.cyan);
      s = grunt.file.read(file);
      this.data.options.forEach(function(item) {
        s = s.replace(new RegExp(item.find), item.repl);
      });
      grunt.file.write(file, s);
    }, this);
  });

  grunt.registerTask('default', [
    'clean:dist',
    'useminPrepare',
    'concurrent:dist',
    'autoprefixer',
    'concat',
    'copy:dist',
    'copy:templates',
    'ngmin',
    'cssmin',
    'uglify',
    'usemin',
    'replace'
  ]);

  grunt.registerTask('debug', function() {
    grunt.config.set('debug', 'true');
    grunt.task.run('default');
  });
};
