<?
require "lib/config.php";
require "lib/params.php";
require "lib/validation.php";
require "lib/db.php";
require "lib/json.php";
require "lib/facebook.php";
require "lib/img.php";


// -- MAIN --
$success = false;
$vars = array();

// check competition is still running:
if(competitionIsClosed()) {
  error(1, 'competition is now closed', true);
}

// check for valid JSON post data:
checkPostData();

// Grab current user's FacebookID from fb-session:
$fb_id = getFBUserId();

// check facebook session is valid:
if(is_null($fb_id)) {
  error(1, 'failed to authorise', true);
}

// Grab Command from request and execute:
$cmd = getCommand();
addToOutput('action', $cmd);

switch($cmd) {

  /* -- SESSION VALIDATION -- */
  case 'verify':
    // nothing to do here - just making sure our fbSession is valid
    $success = true;
    break;

  /* -- SAVE COMPETITION ENTRY -- */
  case 'save':
    $vars = array(
      "first_name" => FILTER_SANITIZE_SPECIAL_CHARS,
      "last_name" =>  FILTER_SANITIZE_SPECIAL_CHARS,
      "phone" => FILTER_SANITIZE_SPECIAL_CHARS,
      "state" => FILTER_SANITIZE_SPECIAL_CHARS,
      "email" => FILTER_SANITIZE_EMAIL,
      "selections" => FILTER_SANITIZE_SPECIAL_CHARS
    );
    $params = getParams($vars);

    // Add to DB:
    $success = insertEntry($fb_id, $params);
    break;

  /* -- GENERATE IMAGE AND POST TO FACEBOOK -- */
  case 'post':
    $vars = array(
      "avatars" => FILTER_UNSAFE_RAW,
      "tags" => FILTER_UNSAFE_RAW
    );
    $params = getParams($vars);

    // Generate image:
    $filename = generateEntryImage($fb_id, $params);
    $success = !is_null($filename);

    // if errors generated from image generation quit here:
    if(hasErrors()) {
      writeResponse($success);
    }

    // send post:
    $result = postToWall($filename,$params['tags']);
    addToOutput('data', $result->asArray());
    $success = true;
    break;

  /* -- CATCH UNKNOWN COMMANDS -- */
  default:
    error(4, 'Unrecognised command');
    $success = false;
    break;
}

// inject entry state into response and write to output:
writeResponse($success);

?>