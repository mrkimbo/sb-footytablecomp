<?

/**
 * Format as CSV and write to output buffer
 */
function writeData($result) {

  if(!isset($result) || $result->num_rows === 0) exit('no results');

  ob_start();
  $df = fopen("php://output", 'w');

  // write row data:
  $first = true;
  while($row = $result->fetch_assoc()) {
    if($first) {
      // write column names:
      $first = false;
      fputcsv($df, array_keys($row));
    }
    // write values:
    fputcsv($df, $row);
  }

  fclose($df);
  return ob_get_clean();
}

/**
 * Write out headers for CSV and file download
 */
function writeHeaders($filename) {
  // disable caching
  $now = gmdate("D, d M Y H:i:s");
  header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
  header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
  header("Last-Modified: {$now} GMT");

  // force download
  header("Content-Type: application/force-download");
  header("Content-Type: application/octet-stream");
  header("Content-Type: application/download");

  // disposition / encoding on response body
  header("Content-Disposition: attachment;filename={$filename}");
  header("Content-Transfer-Encoding: binary");
}

?>