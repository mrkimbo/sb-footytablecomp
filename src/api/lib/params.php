<?
/**
 Error Codes:
  0 - OK
  1 - Auth Error
  2 - SQL Error
  3 - Security Error
  4 - Missing Info
*/

// -------------------- INPUT PARAMETER HELPER METHODS ---------------------- //

// only accept json input:
$post_data = json_decode(
  file_get_contents('php://input'), true
);

/**
 * Make sure we have input
 */
function checkPostData() {
  global $post_data;

  //debug('post_data', $post_data);

  if(json_last_error() !== JSON_ERROR_NONE) {
    error(4, 'malformed input', true);
  }
  if(is_null($post_data)) {
    error(4, 'missing instruction', true);
  }
  if(!isset($post_data['accessToken']) || empty($post_data['accessToken'])) {
    error(4, 'missing token', true);
  }
  if(!isset($post_data['signedRequest']) || empty($post_data['signedRequest'])) {
    error(4, 'missing signedRequest', true);
  }
}

/**
 * Extract 'action' command from input data
 */
function getCommand() {
  global $post_data;

  if(!isset($post_data['action'])) return NULL;

  return strtolower(
    filter_var(
     $post_data['action'],
      FILTER_SANITIZE_SPECIAL_CHARS
    )
  );
}

/**
 * Extract a single parameter from input data by name
 */
function getParam($name, $filter) {
  global $post_data;

  $v = isset($post_data[$name]) ? $post_data[$name] : NULL;
  return validate($name, isset($filter) ? filter_var($v, $filter) : $v);
}

/**
 * Extract collection of values from input data
 */
function getParams($varnames) {
  $p = array();
  foreach($varnames as $name => $filter) {
    $p[$name] = getParam($name, $filter);
  }

  // Exit here if param errors found:
  if(hasErrors()) {
    debug('Params',$p);
    writeResponse(false);
  } else {
    return $p;
  }
}

?>