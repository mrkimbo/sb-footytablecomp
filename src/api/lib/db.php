<?
/**
 Error Codes:
  0 - OK
  1 - Auth Error
  2 - SQL Error
  3 - Security Error
  4 - Missing Info
*/
// ---------------------------- DATABASE HELPERS ---------------------------- //

// DB Connection:
$mysqli = new mysqli(
  $dbConfig['host'],
  $dbConfig['user'],
  $dbConfig['pass'],
  $dbConfig['name']
);

if ($mysqli->connect_errno) {
  error(
    2, "Failed to connect to MySQL {" . $mysqli->connect_errno . "} " .
    $mysqli->connect_error
  );
}

// Execute SQL on DB:
function queryDB($sql) {
  global $mysqli, $debug;

  $result = $mysqli->query($sql);
  if($mysqli->errno){
    error(2, $debug ? $mysqli->error : 'DB Error');
  } else {
    return $result;
  }
}

// Extract last entry per user from Database (with date cap):
function getExtract() {
  global $mysqli, $config;

  $sql = "SELECT entries.id, entries.fb_id, entries.created, first_name, ".
    "last_name, phone, state, email, over_18, selections FROM entries ".
    "INNER JOIN ( SELECT fb_id, MAX(created) as created FROM entries ".
      "WHERE created < '".$config['close_date']."' ".
      "GROUP BY fb_id ) max_date ".
      "ON entries.fb_id = max_date.fb_id ".
      "AND entries.created = max_date.created";

  $result = $mysqli->query($sql);

  if($mysqli->errno){
    exit('DB Error');
  }

  return $result;
}

// Get full db dump:
function getDump() {
  global $mysqli, $config;

  $sql = "SELECT * FROM entries";
  $result = $mysqli->query($sql);

  if($mysqli->errno){
    exit('DB Error');
  }

  return $result;
}

/**
 * Returns db_id for entry
 */
function getEntryId() {
  global $mysqli;
  return $mysqli->insert_id;
}

// ---------------------------- DATABASE ACTIONS ---------------------------- //

/**
 * Add a new record to the entries table
 */
function insertEntry($fb_id, $params) {
  $sql = "INSERT INTO entries ".
    "(fb_id, first_name, last_name, phone, state, email, selections) ".
    "VALUES (".
      "'$fb_id',".
      "'".$params['first_name']."',".
      "'".$params['last_name']."',".
      "'".$params['phone']."',".
      "'".$params['state']."',".
      "'".$params['email']."',".
      "'".$params['selections']."'".
    ");";

  return queryDB($sql);
}

/**
 * Update a record to the entries table based on fb_id
 */
function updateEntry($fb_id, $params) {
  $sql = "UPDATE entries SET ".
    "first_name='".$params['first_name']."',".
    "last_name='".$params['last_name']."',".
    "phone='".$params['phone']."',".
    "state='".$params['state']."',".
    "email='".$params['email']."',".
    "selections='".$params['selections']."',".
    "last_update=NOW(),".
    "update_count=update_count+1 ".
    "WHERE fb_id=$fb_id;";

  return queryDB($sql);
}

/**
 * Extract a record from the entries table based on fb_id
 */
function retrieveEntry($fb_id) {
  global $mysqli, $entryId;

  $sql = "SELECT id, first_name, last_name, phone, state, email, selections ".
    "FROM entries WHERE fb_id=$fb_id";
  $result = queryDB($sql);

  if($mysqli->field_count) {
    $row = $result->fetch_assoc();
    $entryId = $row['id'];
    $result->close();
    return $row;
  }
  return NULL;
}

?>