<?
/**
 Error Codes:
  0 - OK
  1 - Auth Error
  2 - SQL Error
  3 - Security Error
  4 - Missing Info
*/
// -------------------------- VALIDATION HELPERS ---------------------------- //

/**
 * Check if competition has ended
 * (tested 5/8 : Australia/Melbourne timezone)
 */
function competitionIsClosed() {
  global $config;

  //addToOutput('ts',new DateTime());

  $entryCloseDate = new DateTime($config['close_date']);
  $diff = $entryCloseDate->diff(new DateTime('now'));
  //print_r($diff);
  return $diff->invert === 0;
}

/**
 * Validate a value from input data
 */
function validate($name, $value) {
  global $paramValidations;

  // all fields are mandatory:
  if($value === '') {
    error(4, "Param {$name} is undefined");
    return $value;
  }

  switch($name) {
    case 'first_name':
      if(!isValidString($value, 2)) {
        error(4, "Param {$name} is not a valid string of 2 or more chars");
      }
      break;

    case 'last_name':
      if(!isValidString($value, 2)) {
        error(4, "Param {$name} is not a valid string of 2 or more chars");
      }
      break;

    case 'phone':
      if(!isValidPhone($value)) {
        error(4, "Param {$name} is not a valid phone input");
      }
      break;

    case 'state':
      $value = strtoupper($value);
      if(!isValidState($value)) {
        error(4, "Param {$name} is not a valid state");
      }
      break;

    case 'email':
      if(!filter_var($value, FILTER_VALIDATE_EMAIL)){
        error(4, "Param {$name} is not a valid email address");
      }
      break;

    case 'selections':
      // check entries are all ints and has at least 10 selections:
      if(!isValidEntry($value)) {
        error(4, "Param {$name} contains less than 10 items");
      }
      break;

    case 'avatars':
    case 'tags':
    case 'canPost':
      break;


    // unknown parameter:
    default:
      error(4, "unknown parameter {$name}");
  }

  return $value;
}

/**
 * test for empty after removing non-word chars
 */
function isEmpty($val) {
  return empty(preg_replace('/\W/','',$val));
}

/**
 * test for min chars after removing non-word chars
 */
function isValidString($val, $minChars) {
  return strlen(preg_replace('/\W/','',$val)) >= $minChars;
}

$states = array('VIC','SA','WA','NSW','NT','ACT','QLD','TAS');
/**
 * Test for valid Australian state
 */
function isValidState($val) {
  global $states;
  return in_array($val,$states);
}

/**
 * Test for min phone number length (AU land-line: 8digits)
 */
function isValidPhone($val) {
  return isNumeric($val) && strlen($val) >= 8;
}

/**
 * Test for numeric value
 */
function isNumeric($val) {
  //debug('isNumeric',array($val,preg_replace('/[^0-9]/', '', $val)));
  return preg_replace('/[^0-9]/', '', $val) == $val;
}

/**
 * Test for less than 10 entries
 */
function isValidEntry($val) {
  return count(explode(',', $val)) == 10;
}

/**
 * Test for valid url value
 */
function isValidURL($val) {
  return filter_var($val, FILTER_VALIDATE_URL);
}

?>