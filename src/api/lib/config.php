<?

// SITE CONFIG:
$config = array(

  // random future date:
  'close_date' => '2020-01-01 12:00:00',

  // live config:
  'live' => array(
    'db' => array(
      'host' => 'localhost',
      'name' => 'footys14_comp',
      'user' => 'footys14_kimh',
      'pass' => 'Sportsbet1!'
    ),
    'fb' => array(
      'appId' => '604289759690594',
      'secret' => '1bb74a9bda9f4e738f3d67a75db7c38f'
    )
  ),

  // local config:
  'dev' => array(
    'db' => array(
      'host' => 'localhost',
      'name' => 'sb-footytable-comp',
      'user' => 'root',
      'pass' => 'root'
    ),
    'fb' => array(
      'appId' => '616021898517380',
      'secret' => 'ca37b14b8c502eb6882ede18df46d3da'
    )
  )

);

// Detect environment and set appropriate configs:
$live = php_sapi_name() === 'cli' ||
  substr(realpath('./'), 0, 15) === "/home/footys14/";

if($live) {

  // **** LIVE ****

  // allow debug in output:
  $debug = false;

  // specify db + fb config:
  $dbConfig = $config['live']['db'];
  $fbConfig = $config['live']['fb'];

} else {

  // **** LOCAL ****

  // allow debug in output:
  $debug = true;

  // specify db + fb config:
  $dbConfig = $config['dev']['db'];
  $fbConfig = $config['dev']['fb'];
}

?>