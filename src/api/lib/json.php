<?
/**
 Error Codes:
  0 - OK
  1 - Auth Error
  2 - SQL Error
  3 - Security Error
  4 - Parameter Error / Missing Info
*/
// ---------------------------- JSON HELPERS -------------------------------- //

// global output object:
$output = array();

/**
 * Add error to response and exit if critical
 */
function error($type, $msg='', $critical=false) {
  global $output, $debug;

  if(!hasErrors()) $output['error'] = array();
  array_push($output['error'], array(
    'code'=>$type,
    'message'=>$msg
  ));

  if($critical) {
    writeResponse(false);
  }
}

/**
 * If in debug mode, adds debug messages to response
 */
function debug($fn, $msg='') {
  global $output, $debug;
  if(!$debug) return;

  if(!isset($output['debug'])) {
    $output['debug'] = array();
  }
  array_push($output['debug'], array(
    'process' => $fn,
    'message' => isset($msg)?$msg:''
  ));
}

/**
 * Test if errors exist in response
 */
function hasErrors() {
  global $output;
  return isset($output['error']);
}

/**
 * add arbitrary value to response
 */
function addToOutput($prop, $val='') {
  global $output;
  $output[$prop] = $val;
}

/**
 * Output JSON encoded response
 */
function writeResponse($success) {
  global $output;

  header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
  header('Content-type: application/json');

  if(hasErrors()) $success = false;
  addToOutput('success', $success);
  exit(json_encode($output));
}
?>