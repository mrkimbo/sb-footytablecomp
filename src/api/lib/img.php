<?

/**
 Error Codes:
  0 - OK
  1 - Auth Error
  2 - SQL Error
  3 - Security Error
  4 - Missing Info
  5 - Environment/Script Error
*/
// ---------------------------- IMAGE GENERATION ---------------------------- //

$rootFolder = realpath('./img');
$cols = array(226,343,480,615,753,871);
$rows = array(105,242,384);

$positions = array(
  array($cols[1],$rows[0]),
  array($cols[2],$rows[0]),
  array($cols[3],$rows[0]),
  array($cols[4],$rows[0]),
  array($cols[0],$rows[1]),
  array($cols[5],$rows[1]),
  array($cols[1],$rows[2]),
  array($cols[2],$rows[2]),
  array($cols[3],$rows[2]),
  array($cols[4],$rows[2])
);

/**
 * Load and composite facebook avatars onto template image and save
 */
function generateEntryImage($fb_id, $params) {
  global $rootFolder, $positions;

  if (extension_loaded('imagick') !== TRUE) {
    error(5, 'Imagick extension is not loaded.', true);
    return;
  }

  $filename = $rootFolder.'/'.$fb_id.'.jpg';

  // load template:
  $template = new Imagick();
  $template->readImage($rootFolder.'/template.png');


  $friends = explode(',',$params['avatars']);
  if(count($friends) < 10) {
    //error(4, "Param {avatars} contains less than 10 items", true);
  }

  // add avatars:
  $pos = NULL;
  foreach($friends as $idx => $friend) {
    $pos = $positions[$idx];
    addAvatar($template, $friend, $pos[0], $pos[1]);
  }

  $template->writeImage($filename);
  return $filename;
}

/**
 * Composite a single avatar into the template image
 */
function addAvatar($template, $url, $x, $y) {
  //debug('avatar', $url);
  $img = new Imagick();
  try {
    $img->readImage(rawurldecode($url));
    $img->resizeImage(104, 104, Imagick::FILTER_UNDEFINED, 0.8);
    $template->compositeImage($img, Imagick::COMPOSITE_DEFAULT, $x, $y);
  } catch(Exception $err) {
    error(5, $err->getMessage());
  }
}

/**
 * Read the Exif data in an entry image
 */
function examineEntry() {
  global $fb_id, $rootFolder;

  $img = new Imagick();
  try {
    $img->readImage($rootFolder.'/'.$fb_id.'.jpg');
  } catch( Exception $ex) {
    addToOutput('no entry img found for current user');
    return false;
  }

  addToOutput('exif', array(
    'Author' => $img->getImageProperty('Exif:Author'),
    'MakerNote' => $img->getImageProperty('Exif:MakerNote')
  ));

  return true;
}

?>
