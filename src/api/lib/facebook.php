<?
/**
 Error Codes:
  0 - OK
  1 - Auth Error
  2 - SQL Error
  3 - Security Error
  4 - Missing Info
*/

require "fb-sdk/autoload.php";

use Facebook\FacebookJavascriptLoginHelper;
use Facebook\FacebookRequestException;
use Facebook\FacebookRequest;
use Facebook\FacebookSession;
use Facebook\Entities\SignedRequest;
use Facebook\GraphUser;
use Facebook\GraphObject;

$_SESSION_ID = 'fb-session';
$fbSession = NULL;

/**
 * Create facebook session from passed tokens
 */
function getFBSession($token, $signedRequest) {
  global $_SESSION_ID, $fbSession, $fbConfig;

  FacebookSession::setDefaultApplication(
    $fbConfig['appId'],
    $fbConfig['secret']
  );

  try {
    $fbSession = new FacebookSession(
      $token,
      new SignedRequest($signedRequest)
    );
  } catch(FacebookRequestException $ex) {
    error(1, $ex->getMessage(), true);
  }
}

/**
 * Extract FacebookID from current session:
 */
function getFBUserId() {
  global $post_data, $fbSession;

  // use fb-auth-token from post request:
  $token = $post_data['accessToken'];
  $sr = $post_data['signedRequest'];

  // get session and check user:
  getFBSession($token, $sr);

  try {
    if(!$fbSession) throw new Exception();
    $fbSession->validate();
  } catch (Exception $ex) {

    error(0, $ex->getMessage());
    return NULL;
  }

  // valid session - return user id:
  return $fbSession->getUserId();
}

/**
 * Post an entry image to the user's facebook wall
 * @param {string} imgPath Path to generated image
 * @param {array<string>} tags Array of friend tokens
 */
function postToWall($imgPath, $tags) {
  global $fbSession;

  $rawTags = explode(',',trim($tags));
  $tags = array();
  foreach($rawTags as $tag) {
    array_push($tags, "{ 'tag_uid': '$tag' }");
  }

  // ToDo: Remove privacy property in submission!
  try {
    $request = new FacebookRequest(
      $fbSession,
      'POST',
      '/me/photos',
      array(
        'source'  => new CURLFile($imgPath, 'image/jpg'),
        'place'   => '113208555377897',
        'tags'    => '['.(implode(',', $tags)).']'
      )
    );
    $response = $request->execute();
    return $response->getGraphObject();

  } catch(Exception $ex) {
    // api call failed:
    error(0, $ex->getMessage());
  }

  return false;
}

?>