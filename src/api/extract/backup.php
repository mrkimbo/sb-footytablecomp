<?
require '/home/footys14/public_html/api/lib/config.php';
require '/home/footys14/public_html/api/lib/db.php';
require '/home/footys14/public_html/api/lib/io.php';

$d = (new DateTime())->format('dmY-His');
$filename = "backup-$d.csv";

// Save Full Dump:
$content = writeData(getDump());
$file = fopen(realpath('/home/footys14/public_html/api/extract/cache')."/$filename", 'w');
fwrite($file, $content);
fclose($file);

// optional download:
if(isset($_REQUEST['download'])) {
  writeHeaders($filename);
  echo $content;
}
?>