/**
 * Created with JetBrains PhpStorm.
 * User: kimh
 * Date: 13/09/13
 * Time: 11:23 AM
 */

(function(){

  'use strict';

  /**
   * QuerySelector helpers that automatically return results as iterable arrays
   */
  /*Element.prototype.find = document.find = function() {
    return this.querySelector.apply(this, arguments);
  };
  Element.prototype.findAll = document.findAll = function() {
    var r = this.querySelectorAll.apply(this, arguments);
    return r.length ? Array.prototype.constructor.apply(null,r) : [];
  };*/

  var Utils = {

    /**
     * Reports whether touch interaction is available.
     * @returns {boolean}
     */
    isTouchEnabled: function() {
      return !!('ontouchstart' in window);
    },

    /**
     *Check if we're in Internet Explorer
     */
    isIE: function() {
      return (/MSIE/i).test(navigator.userAgent);
    },

    /**
     * Check if we're in IE9 or later
     * @return {boolean}
     */
    gteIE9: function() {
      if(!this.isIE()) return true;
      var ver = navigator.userAgent.match(/MSIE ([0-9]+)/);
      return ver ? parseInt(ver[1]||9) >= 9 : false;
    },

    /**
     * Check for chrome on iOS
     * @return {boolean}
     */
    isChromeIOS: function() {
      return (/i(pad|phone)/i).test(navigator.userAgent) &&
      (/CriOS/i).test(navigator.userAgent);
    },

    /**
     * Replacement for GSAP CSSPlugin as it's 32k.
     * Applies 'px' based CSS values to tween targets.
     * Note: Only supports px,% and deg units. Initial values of 'auto' are
     * treated as 'px'
     */
    applyCSS: function() {
      var i, v, s, props = this._propLookup;
      for(i in props) {
        if(props.hasOwnProperty(i)) {
          // if first time accessed, store initial value and eval units:
          if(props[i].start === undefined) {
            s = getComputedStyle(this._propLookup[i].t)[i];
            if(s === 'auto') {
              props[i].unit = 'px';
            } else {
              props[i].unit = s.match(/[^0-9]+$/i) || '';
            }
            props[i].start = parseFloat(s) || 0.0;
          }
          v = props[i].start + ((this.vars[i] - props[i].start) * this.ratio);
          /*log(props[i].start + '[' + props[i].unit + '] -> ' + this.vars[i] +
           '*' + this.ratio + ': ' + v
           );*/
          this.target.style[i] = v + props[i].unit;
        }
      }
    },

    getElementOffset: function( el ) {
      var _x = 0;
      var _y = 0;
      while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
        _x += el.offsetLeft - el.scrollLeft;
        _y += el.offsetTop - el.scrollTop;
        el = el.offsetParent;
      }
      return { top: _y, left: _x };
    }
  };

  /**
   * Logging helper.
   * @param {...*} args
   */
  window.log = function() {
    if(!window.console || !window.console.log || window.DEBUG === false) {
      return;
    }
    // ie9 fix:
    if(typeof console.log === 'object') {
      Function.prototype.bind.call(console.log,console)
        .apply(console,arguments);
    } else {
      console.log.apply(console,arguments);
    }
  };

  // drop into window scope:
  window.utils = Utils;

})();
