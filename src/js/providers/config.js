/**
 * Created with JetBrains PhpStorm.
 * User: kimh
 * Date: 16/07/14
 * Time: 12:18 PM
 */

/* global angular, faker */

angular.module('App').value('Config', {

  LIVE: true,

  API: '/api/',

  URL: {
    TERMS: 'http://www.sportsbet.com.au/blog/sportsbet-offers-promotions-stuff/free-competitions/win-sportsbets-footy-show-table-terms-conditions',
    PROMO: 'http://ad.doubleclick.net/ddm/clk/283701638;110526924;d'
  },

  ID: {
    test  : '616021898517380',
    live  : '604289759690594',
    sbPage: '113208555377897'
  },

  State: {

    Screen: {
      LANDING     : 'html/landing.html',
      SELECTION   : 'html/selection.html',
      CONFIRMATION: 'html/confirm.html'
    },

    FB: {
      NOT_SET   : -1,
      UNKNOWN   : 0,
      FB_AUTH   : 1,
      APP_AUTH  : 2
    }
  },

  Event: {
    APP_ERROR         : 'App:AuthError',
    ALERT_SHOW        : 'Alert:Show',
    ALERT_HIDE        : 'Alert:Hide',
    FB_MODEL_UPDATED  : 'FBModel:Updated'
  },

  Messages: {

    GAMBLE: 'Gamble Responsibly',
    PERMIT: 'NSW Permit No. LTPS/14/05529',

    ALERT_FATAL: {
      title: 'D\'OH!',
      message: 'Something went wrong. Give it another crack.',
      buttonLabel: 'OK'
    },
    ALERT_BROWSER: {
      title: 'Your browser is older than the Foss',
      message: 'Enter the draw by ugrading to the latest version.',
      buttonLabel: 'UPGRADE'
    },
    ALERT_SELECTIONS: {
      title: 'Too many invites',
      message: 'Two\'s company, three\'s a crowd,<br/> ten\'s a table!<br/>' +
        'Remove someone to add another.',
      buttonLabel: 'OK'
    },
    ALERT_SUBMIT: {
      title: 'Submitting',
      message: 'Please wait while we enter you in the draw.'
    },
    ALERT_FB_POST: {
      title: 'Posting to Facebook',
      message: 'Please wait while we share your table with your mates.'
    },

    VALIDATION: {
      first_name  : 'We need your real first name. No 1337 speak, symbols or nicknames.',
      last_name   : 'We need your real last name. No 1337 speak, symbols or nicknames.',
      email       : 'We need your email so we can contact you if you win',
      state       : 'Where the bloody hell are you? Select your state.',
      phone       : 'We need a landline/mobile so we can contact you if you win (incl. area code)',
      checkboxes  : 'You must be 18+ and agree to our T&Cs to enter.',
      like        : 'Press the like button!',
      default     : 'Please review the highlighted fields.'
    }

  },

  /**
   * Generate test friends using fakerJS
   * @param {number} count
   * @return {Array}
   */
  getDummyFriends: function(count) {
    'use strict';
    var arr = [];
    if(!window.hasOwnProperty('faker')) return arr;
    while(arr.length < count) {
      arr.push({
        'id': faker.Image.avatar() + '' + faker.random.number(),
        'name': faker.Name.firstName() + ' ' + faker.Name.lastName(),
        'picture': {
          'data': {
            'is_silhouette': false,
            'url': faker.Image.avatar()
          }
        }
      });
    }
    return arr;
  },

  /**
   * Generate test fb profile
   * @return {Object}
   */
  getDummyProfile: function() {
    'use strict';
    return {
      id: '10152173754811090',
      first_name: 'Dummy',
      gender: 'male',
      last_name: 'McFakename',
      link: 'https://www.facebook.com/app_scoped_user_id/10152173754811090/',
      locale: 'en_GB',
      name: 'Dummy McFakename',
      timezone: 10,
      updated_time: new Date().toISOString(),
      verified: true,
      profileImg: 'img/test-avatar.jpg',
      email: 'test@email.com'
    };
  }

});