/**
 * Created with WebStorm.
 * User: kimh
 * Date: 17/07/2014
 * Time: 9:53
 */

/* global angular, FB, log */

angular.module('App').service('FacebookService', function($rootScope, Config) {

  'use strict';

  this.statusCallback = null;
  this.apiSuccessHandler = null;

  this.init = function(appId) {
    log(this + '::init()');
    var options = {
      version:  'v2.0',
      appId:    appId,
      cookie:   true,
      status:   true,
      xfbml:    true,
      channel:  'http://www.footyshowtable.com/api/channel.api'
    };
    FB.init(options);
  };

  this.getLoginStatus = function() {
    log(this + '::getLoginStatus()');
    FB.getLoginStatus(this.statusCallback);
  };

  this.login = function(perms) {
    FB.login(this.statusCallback, {scope: perms});
  };

  this.logout = function() {
    FB.logout(null);
  };

  this.getBasicInfo = function() {
    FB.api(
      '/me',
      angular.bind(this, _onAPISuccess, 'basic-info')
    );
  };

  this.getProfileImage = function() {
    FB.api(
      '/me/picture?redirect=0&width=50&height=50',
      angular.bind(this, _onAPISuccess, 'profile-img')
    );
  };

  this.getTaggableFriends = function() {
    FB.api(
      '/me/taggable_friends',
      angular.bind(this, _onAPISuccess, 'friends')
    );
  };

  this.checkSBLike = function() {
    FB.api(
      '/me/likes/' + Config.ID.sbPage,
      angular.bind(this, _onAPISuccess, 'check-sb-like')
    );
  };

  this.checkPermissions = function() {
    FB.api(
      '/me/permissions',
      angular.bind(this, _onAPISuccess, 'perms')
    );
  };

  this.toString = function() {
    return 'FacebookService';
  };


  function _onAPISuccess(type, data) {
    this.apiSuccessHandler.apply(null, arguments);
    $rootScope.$digest();
  }

});
