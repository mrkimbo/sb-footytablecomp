/**
 * Created with PhpStorm.
 * User: kimh
 * Date: 16/07/14
 * Time: 10:25 AM
 */

/* global angular, ga, log, Enabler */

angular.module('App').service('Metrics',function(Config){

  'use strict';

  this._impressions = {};
  this.Event = {

    TABLE_FILLED: 'table-filled',
    FORM_SHOWN: 'form-shown',
    FORM_HIDDEN: 'form-hidden',
    FB_PAGE_LIKED: 'fb-page-liked',
    FB_CAN_PUBLISH: 'fb-can-publish',
    ENTRY_SUBMITTED: 'entry-submitted',
    POST_PUBLISHED: 'wall-post-published',
    SUBMIT_POST_CLICKED: 'submit-post-clicked',
    TERMS_CLICKED: 'terms-clicked',
    PROMO_CLICKED: 'promo-clicked'
  };

  /**
   * Retrieve number of times a counter has been fired.
   * @param {String} type
   * @returns {number}
   */
  this.getImpressionCount = function(type) {
    return this._impressions[type] || 0;
  };

  /**
   * Dispatch GA pageview for implied pages.
   * Ensures values have leading slash
   * @param name
   */
  this.trackPageView = function(name) {
    log(this + '::trackPageView(' + name + ')');
    ga('send', 'pageview', name.replace(/^\/?/,'/'));
  };

  /**
   * Dispatch GA event.
   * @param {String} type
   */
  this.trackEvent = function(name, maxImpressions) {
    // suppress unwanted counters //
    if(this.getImpressionCount(name) >= (maxImpressions||999)) {
      return;
    }

//    log(this + '::trackEvent(' + name + ')');
    ga('send', 'event', 'click', name);

    // log impression //
    this._impressions[name] = (this._impressions[name]||0)+1;
  };

  this.trackError = function(name, isFatal) {
    //log(this + '::trackError(' + name + ',' + isFatal + ')');

    ga('send', 'exception', {
      'exDescription': name,
      'exFatal': isFatal
    });
  };

  this.toString = function() {
    return 'Metrics';
  };

});