/**
 * Created with WebStorm.
 * User: kimh
 * Date: 16/07/2014
 * Time: 17:01
 */

/* global angular, log */

angular.module('App').service('APIService', function(
  $rootScope, $q, $http, $timeout, Config) {

  'use strict';

  var config = {
    url: Config.API,
    method: 'POST',
    responseType: 'json'
  };

  this.apiSuccessHandler = null;
  this.apiErrorHandler = null;
  this.auth = null;

  /**
   * Save an entry to the db.
   * Don't need to specify insert/update as backend automatically checks
   * for pre-existing entry.
   * @param {Object} entry
   */
  this.save = function(entry) {
    log(this + '::save()');
    this.executeRequest('save', entry);
  };

  /**
   * Post a generated entry image to users' facebook timeline.
   * @param {Object} info
   */
  this.submitPost = function(info) {
    log(this + '::save()');
    this.executeRequest('post', info);
  };

  /**
   * Check with server that valid session exists
   */
  this.verifySession = function() {
    log(this + '::verifySession()');
    this.executeRequest('verify');
  };

  /**
   * Execute a server request and bind result callbacks
   * @param {string} type
   * @param {object} data
   */
  this.executeRequest = function(type, data) {
    log(this + '::executeRequest(' + type + ')');

    if(!Config.LIVE) {
      var _this = this;
      setTimeout(function() {
        _onAPISuccess.call(_this, type, { success: true });
      }, 500);
      return;
    }

    $http(createPayload(type, this.auth, data))
      .success(angular.bind(this, _onAPISuccess, type))
      .error(angular.bind(this, _onAPIError, type));
  };

  /**
   * Class identifier
   * @return {string}
   */
  this.toString = function() {
    return 'APIService';
  };


  // --------------------------- PRIVATE HELPERS ---------------------------- //
  /**
   * Wrap action, token and data in 'data' object for posting to API
   * @param {string} action
   * @param {string} token
   * @param {Object?} data
   * @return {Object}
   */
  function createPayload(action, token, data) {
    var obj = angular.copy(config);
    obj.data = angular.extend(
      angular.extend(data || {}, token),
      { 'action': action }
    );

    //log('createPayload()', obj);
    return obj;
  }

  /**
   * Handle API response.
   * Executed in APIService scope.
   * @param {string} type
   * @param {Object} data
   * @private
   */
  function _onAPISuccess(type, data) {
    var _this = this;
    $timeout(function() {
      $rootScope.$apply(
        _this.apiSuccessHandler.call(null, type, data)
      );
    });
  }

  /**
   * Handle errors returned from calling the API.
   * Executed in APIService scope.
   * @param {string} type
   * @param {Object} data
   * @private
   */
  function _onAPIError(type, data) {
    var _this = this;
    $timeout(function() {
      $rootScope.$apply(
        _this.apiErrorHandler.call(null, type, data)
      );
    });
  }

});
