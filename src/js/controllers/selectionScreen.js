/**
 * Created with WebStorm.
 * User: kimh
 * Date: 17/07/2014
 * Time: 11:23
 */

/* global utils, log, angular, FB, TweenLite, Quad */

(function() {

  'use strict';

  var $scope, $rootScope, $timeout, $window, Config, Metrics;
  var dependencies = [
    '$scope', '$rootScope', '$timeout', '$window', '$element',
    'Config', 'FBModel', 'AppModel', 'Metrics'
  ];

  function SelectionScreenController(
    _$scope, _$rootScope, _$timeout, _$window, $element,
    _Config, FBModel, AppModel, _Metrics
    ) {

    // globals:
    Config = _Config;
    Metrics = _Metrics;
    $scope = _$scope;
    $rootScope = _$rootScope;
    $timeout = _$timeout;
    $window = _$window;

    // class props:
    this._el = $element;
    this._appModel = AppModel;
    this._fbModel = FBModel;

    // scope props:
    $scope.name = 'SelectionScreen';
    $scope.friendFilter = '';
    $scope.filteredFriends = [];
    $scope.friendListHeight = 0;
    $scope.formHeight = 0;
    $scope.formIsVisible = false;

    // scope methods:
    $scope.getSeatImage = angular.bind(this, this.getSeatImage);
    $scope.seatTaken = angular.bind(this, this.seatTaken);
    $scope.tableIsFull = angular.bind(this, this.tableIsFull);
    $scope.toggleFriend = angular.bind(this, this.toggleFriend);
    $scope.clearSeat = angular.bind(this, this.clearSeat);
    $scope.isFriendSelected = angular.bind(this, this.isFriendSelected);
    $scope.focusInput = angular.bind(this, this.focusInput);
    $scope.showEntryForm = angular.bind(this, this.showEntryForm);
    $scope.clearFilter = angular.bind(this, this.clearFilter);
    $scope.tableIsFull = angular.bind(
      this._appModel, this._appModel.tableIsFull
    );

    this.init();
    this._onResize();
  }

  SelectionScreenController.prototype = {

    /**
     * Initialisation
     */
    init: function() {
      log(this + '::init()');
      this._getElements();
      this._addListeners();

      // Check user is logged in and has connected:
      if(!this._fbModel.isAuthorised()) {
        this._showLandingScreen();
      } else {
        this._fbModel.fetchUserInfo();
        this._appModel.verifySession();
      }

      // hide reveal overlay:
      $scope.API.reveal.hide();
    },

    /**
     * Store refs to dom elements for later use
     * @private
     */
    _getElements: function() {
      //log(this + '::getElements()');
      this._inputField = this._el[0].querySelector('#search-input');
      this._friendList = this._el[0].querySelector('.friend-list');
      this._centerBar = this._el[0].querySelector('.center-bar');
      this._content = this._el[0].querySelector('.lower-content');
    },

    /**
     * Add watchers and event listeners
     * @private
     */
    _addListeners: function() {
      var _this = this;

      $scope.$watch('fbModel.userInfo.profileImg', function(newvalue) {
        if(!angular.isDefined(newvalue)) return;
        // Fill user seat:
        _this._appModel.selections[4] = {
          name: _this._fbModel.userInfo.name,
          img: newvalue
        };
      });
      $scope.$watch(
        'fbModel.friends', angular.bind(this, this._onFriendsListPopulated)
      );
      $scope.$watch(
        'friendFilter', angular.bind(this, this._applyFriendFilter)
      );

      $scope.$watch('windowWidth', angular.bind(this, this._onResize));
      $scope.$watch('windowHeight', angular.bind(this, this._onResize));

      // when entry form content loaded, store element refs:
      $scope.$on('$includeContentLoaded', angular.bind(this, function() {
        this._form = this._el[0].querySelector('#entry-form');
      }));

      $scope.$watch(
        function(){ return $scope.tableIsFull(); },
        angular.bind(this, this.hideEntryForm)
      );
    },

    /**
     * Filter friend list based on input
     * @private
     */
    _applyFriendFilter: function() {
      //log(this + '::applyFriendFilter()');
      $scope.filteredFriends.length = 0;
      var input = ($scope.friendFilter||'').toLowerCase();
      angular.forEach(this._fbModel.friends, function(friend) {
        if(input === '' || friend.name.toLowerCase().indexOf(input) === 0) {
          $scope.filteredFriends.push(friend);
        }
      });
      //log($scope.filteredFriends);
    },

    /**
     * Handle friend list info response from fb
     * @param {Array} newvalue
     * @private
     */
    _onFriendsListPopulated: function(newvalue) {
      if(!newvalue) return;

      this._applyFriendFilter();
      this._onResize();
    },

    /**
     * Handle window resize events:
     * @private
     */
    _onResize: function() {

      /*var h = $scope.windowHeight - utils.getElementOffset(this._content).top;
      if(!this._inputHeight) this._inputHeight = 53;
      if(!this._centerBarHeight) this._centerBarHeight =
        parseInt(window.getComputedStyle(this._centerBar).height);

      $scope.contentHeight = h;
      $scope.listHeight = h - this._inputHeight - this._centerBarHeight;*/
    },

    /**
     * User not authorised to be here - exit to landing page:
     * @private
     */
    _showLandingScreen: function() {
      this._appModel.viewState = Config.State.Screen.LANDING;
    },

    /**
     * Initialise and show the entry form
     * @param {object} evt
     */
    showEntryForm: function(evt) {
      log(this + '::showEntryForm()');
      if(!evt || (/disabled/).test(evt.target.className)) return;

      $scope.formIsVisible = true;

      Metrics.trackEvent(Metrics.Event.FORM_SHOWN);

      // wrap in timeout as scrolltop and FB.parse will only work if
      // content is displayed.
      $timeout(
        angular.bind(this, function() {
          this._form.scrollTop = 0;
          FB.XFBML.parse();
        })
      );
    },

    /**
     * Hide the entry form
     * @param {boolean} newvalue
     */
    hideEntryForm: function(oval, nval) {
      if(oval === nval) return;
      if($scope.formIsVisible && nval) {
        Metrics.trackEvent(Metrics.Event.FORM_HIDDEN);
        $scope.formIsVisible = false;
      }
    },

    /**
     * Set focus to the input field when user clicks the top bar
     */
    focusInput: function() {
      this._inputField.focus();
    },

    /**
     * Clear friend list filter and scroll to top
     */
    clearFilter: function() {
      $scope.friendFilter = '';
      this._friendList.scrollTop = 0;
    },

    /**
     * Check if friend is selected (from friend-list):
     * @param {String} id
     * @return {boolean}
     */
    isFriendSelected: function(id) {
      for(var i=0; i<this._appModel.selections.length; i++) {
        if(!angular.isDefined(this._appModel.selections[i])) continue;
        if(this._appModel.selections[i].id === id) {
          return true;
        }
      }
      return false;
    },

    /**
     * Toggle selected state of friend (from friend-list):
     * @param {Object} friend
     */
    toggleFriend: function(friend) {

      if(this.isFriendSelected(friend.id)) {
        this._appModel.deselectFriend(friend);
      } else {

        // if table is full and trying to add another, show warning:
        if(this.tableIsFull()) {
          $rootScope.$broadcast(Config.Event.ALERT_SHOW, {
            title: Config.Messages.ALERT_SELECTIONS.title,
            message: Config.Messages.ALERT_SELECTIONS.message,
            buttonLabel: Config.Messages.ALERT_SELECTIONS.buttonLabel,
            state: 'button'
          });

        } else {
          this._appModel.selectFriend(friend);
        }
      }
    },

    /**
     * Retrieve image for slot.
     * @param {number} seat_idx
     * @return {string}
     */
    getSeatImage: function(seat_idx) {
      var unselectedImg = 'img/unselected.png';
      if(seat_idx === 4) {
        return this._fbModel.userInfo && this._fbModel.userInfo.profileImg ?
          this._fbModel.userInfo.profileImg :
          unselectedImg;
      }

      return this.seatTaken(seat_idx) ?
        this._appModel.selections[seat_idx].img :
        unselectedImg;
    },

    /**
     * Check if slot is empty:
     * @param {number} seat_idx
     * @return {boolean}
     */
    seatTaken: function(seat_idx) {
      return angular.isDefined(this._appModel.selections[seat_idx]);
    },

    /**
     * Clear a seat at the table:
     * @param {number} seat_idx
     */
    clearSeat: function(seat_idx) {
      delete this._appModel.selections[seat_idx];
    },

    /**
     * Check whether all seats around the table are taken
     * @return {boolean}
     */
    tableIsFull: function() {
      for(var i=0; i<this._appModel.selections.length; i++) {
        if(!this.seatTaken(i)) return false;
      }
      return true;
    },

    /**
     * Class identifier
     * @return {string}
     */
    toString: function() {
      return 'SelectionScreenController';
    }
  };


  SelectionScreenController.$inject = dependencies;
  angular.module('App').controller(
    'SelectionScreenController', SelectionScreenController
  );

})();
