/**
 * Created with WebStorm.
 * User: kimh
 * Date: 25/07/2014
 * Time: 15:14
 */

/* global log, utils, angular */

(function() {

  'use strict';

  var $scope, Config, Metrics;
  var dependencies = [
    '$scope', 'Config', '$element', 'AppModel', 'FBModel', 'Metrics'
  ];

  var EntryForm = function(
    _$scope, _Config, $element, AppModel, FBModel, _Metrics
    ) {

    // globals:
    Config = _Config;
    $scope = _$scope;
    Metrics = _Metrics;

    // class props:
    this._el = $element;
    this._appModel = AppModel;
    this._fbModel = FBModel;
    this._submitted = false;

    // scope props:
    $scope.termsAccept = false;
    $scope.ageAccept = true;
    $scope.termsUrl = Config.URL.TERMS;

    // scope methods:
    $scope.isEntryComplete = angular.bind(this, this.isEntryComplete);
    $scope.submitEntry = angular.bind(this, this.submitEntry);
    $scope.getFormError = angular.bind(this, this.getFormError);
    $scope.showTerms = angular.bind(this, this.showTerms);

    this.init();
  };

  EntryForm.prototype = {

    /**
     * Initialisation
     */
    init: function() {
      this._getElements();
      this._addListeners();
    },

    /**
     * Store refs to DOM elements for later use
     * @private
     */
    _getElements: function() {
      this._form = this._el.find('form')[0];

      this._fields = {
        first_name: angular.element(this._form.input_first_name),
        last_name: angular.element(this._form.input_last_name),
        email: angular.element(this._form.input_email),
        state: angular.element(this._form.input_state),
        phone: angular.element(this._form.input_phone)
      };
    },

    /**
     * Add watchers and event listeners
     * @private
     */
    _addListeners: function() {},

    /**
     * Validate form fields and show error messages
     * @private
     */
    _validateFields: function() {
      //log(this + '::validateFields()');

      var input;
      angular.forEach(this._fields, function(field) {
        input = $scope.entryForm[field.attr('name')];
        if(!input.$valid) {
          field.removeClass('ng-pristine');
        }
      }, this);

      // mark form as dirty:
      $scope.entryForm.$setDirty();
    },

    /**
     * Show the appropriate error message
     * @return {string}
     */
    getFormError: function() {
      //log(this + '::getFormError()');

      // if user hasn't interacted, don't show errors:
      if($scope.entryForm.$pristine) return '';

      // check fields:
      var input, name;
      for(var i in this._fields) {
        name = this._fields[i].attr('name');
        input = $scope.entryForm[name];
        if(input && !input.$valid && !this._fields[i].hasClass('ng-pristine')) {
          return Config.Messages.VALIDATION[name.replace('input_','')] ||
            Config.Messages.VALIDATION.default;
        }
      }

      // if user hasn't submitted form, only validate edited fields:
      if(!this._submitted) return '';

      // check terms and age checkboxes:
      if(!$scope.termsAccept || !$scope.ageAccept) {
        return Config.Messages.VALIDATION.checkboxes;
      }

      // all good:
      return '';
    },

    /**
     * Check if we have a complete entry
     * @return {boolean}
     */
    isEntryComplete: function() {
      return $scope.entryForm.$valid &&
        $scope.ageAccept &&
        $scope.termsAccept &&
        this._appModel.entryComplete();
    },

    /**
     * Show terms and conditions and record metric
     */
    showTerms: function() {
      Metrics.trackEvent(Metrics.Event.TERMS_CLICKED);
      return true;
    },

    /**
     * Request extended permissions if required and submit entry
     */
    submitEntry: function() {
      if (!this.isEntryComplete()) {
        this._validateFields();
        this._submitted = true;
        return;
      }

      log(this + '::submitEntry()');
      this._appModel.save();
    },

    /**
     * Class identifier
     * @return {string}
     */
    toString: function() {
      return 'EntryForm';
    }

  };


  EntryForm.$inject = dependencies;
  angular.module('App').controller('EntryForm', EntryForm);

})();
