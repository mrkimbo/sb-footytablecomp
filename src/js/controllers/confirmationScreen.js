/**
 * Created with WebStorm.
 * User: kimh
 * Date: 17/07/2014
 * Time: 11:23
 */

/* global angular, utils, log */

(function() {

  'use strict';

  var $scope, Config, Metrics;
  var dependencies = [
    '$scope', '$element', 'Config', 'AppModel', 'FBModel', 'Metrics'
  ];

  var ConfirmScreenController = function(
    _$scope, $element, _Config, AppModel, FBModel, _Metrics
    ) {

    // No functionality required
    // globals:
    Config = _Config;
    $scope = _$scope;
    Metrics = _Metrics;

    // class props:
    this._el = $element;
    this._appModel = AppModel;
    this._fbModel = FBModel;

    // scope props:

    // scope methods:
    $scope.postToFacebook = angular.bind(this, this.postToFacebook);

    this.init();
  };

  ConfirmScreenController.prototype = {

    init: function() {
      this._getElements();
      this._addListeners();

      // hide reveal overlay:
      $scope.API.reveal.hide();
    },

    _getElements: function() {},

    /**
     * Add watchers and event listeners
     * @private
     */
    _addListeners: function() {

      $scope.$on(
        Config.Event.FB_MODEL_UPDATED,
        angular.bind(this, this._onFBModelUpdated)
      );

    },

    /**
     * Check for perms and submit request to generate image and post
     * to FB timeline.
     */
    postToFacebook: function() {
      // user has already been asked for publish perms if on chrome iOS
      // so don't ask again.
      if(this._fbModel.canPost || utils.isChromeIOS()) {
        this._appModel.postToFacebook();
      } else {
        this._fbModel.requestPublishPerms();
      }
    },

    /**
     * Watcher for fbModel.canPost
     * Submits postToFacebook request
     * @param {object} evt
     * @param {string} type
     * @private
     */
    _onFBModelUpdated: function(evt, type) {
      log(this + '::onFBModelUpdated(' + type + ')');
      // perms has been updated in the model - continue posting to fb:
      if(type === 'perms' && this._fbModel.canPost) {
        this._appModel.postToFacebook();
      }
    },

    /**
     * Class identifier
     * @return {string}
     */
    toString: function() {
      return 'ConfirmScreenController';
    }

  };

  ConfirmScreenController.$inject = dependencies;
  angular.module('App').controller(
    'ConfirmScreenController', ConfirmScreenController
  );

})();
