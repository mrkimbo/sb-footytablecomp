/**
 * Created with WebStorm.
 * User: kimh
 * Date: 17/07/2014
 * Time: 11:21
 */

/* global log, angular */

(function() {

  'use strict';

  var Config, $timeout, $scope, $rootScope;
  var dependencies = [
    '$scope', '$element', '$timeout', 'Config',
    'FBModel',  'AppModel', '$rootScope'
  ];

  function LandingScreenController(
    _$scope, $element, _$timeout, _Config, FBModel, AppModel, _$rootScope
    ) {

    // globals:
    Config = _Config;
    $scope = _$scope;
    $timeout = _$timeout;
    $rootScope = _$rootScope;

    // class props:
    this._el = $element;
    this._appModel = AppModel;
    this._fbModel = FBModel;
    this._enterBtnClicked = false;

    // scope props:
    $scope.enterBtnImg = 'img/btn-facebook-loading.png';
    $scope.name = 'LandingScreen';

    // scope refs:
    $scope.enterApp = angular.bind(this, this.onEnterBtnClick);

    this.init();
  }

  LandingScreenController.prototype = {

    /**
     * Initialisation
     */
    init: function() {
      log(this + '::init()');

      this._getElements();
      this._addListeners();

      if(this._appModel.abortApp) {
        this._onAbort();
        return;
      }

      // check fb model not already intialised:
      if(this._fbModel.state !== Config.State.FB.NOT_SET) {
        this._onFBModelUpdated();
      }

      // hide reveal overlay:
      $scope.API.reveal.hide();
    },

    /**
     * Store refs to DOM elements for later use
     * @private
     */
    _getElements: function() {
      // store references to view elements
      this._enterBtn = angular.element(
        this._el[0].querySelector('.button')
      );
    },

    /**
     * Add watchers and event listeners
     * @private
     */
    _addListeners: function() {
      // watch facebook user state and fb basic info:
      log(this + '::addListeners()');

      $scope.$watch('windowWidth', angular.bind(this, this._onResize));
      $scope.$watch('windowHeight', angular.bind(this, this._onResize));
      $scope.$on(
        Config.Event.FB_MODEL_UPDATED,
        angular.bind(this, this._onFBModelUpdated)
      );
    },

    /**
     * Disable enter button if browser fails checks
     * @private
     */
    _onAbort: function() {
      log(this + '::abort()');
      this._enterBtn.toggleClass('disabled', true);
    },

    /**
     * Handle window resize events:
     * @private
     */
    _onResize: function() {
      //this._footer.style.height = $scope.windowHeight -
      //  utils.getElementOffset(this._footer).top + 'px';
    },

    /**
     * Handle FBModel updates:
     * @param {Object} evt
     * @param {string} type
     * @private
     */
    _onFBModelUpdated: function(evt, type) {
      log(this + '::onFBModelUpdated(' + type + ')');
      if(this._appModel.abortApp) return;

      if(this._fbModel.isAuthorised() && this._enterBtnClicked) {
        this._showSelectionScreen();
      }
    },

    /**
     * Handle enter button clicks
     */
    onEnterBtnClick: function() {
      if(this._enterBtn.hasClass('disabled')) return;
      this._enterBtnClicked = true;

      if(this._fbModel.isAuthorised()) {
        this._showSelectionScreen();
      } else {
        this._fbModel.login();
      }
    },

    /**
     * Exit to selection screen:
     * @private
     */
    _showSelectionScreen: function() {
      //log(this + '::showSelectionScreen()');
      // pre-populate entry form with FB details if available:
      if(!this._fbModel.userInfo) return;
      this._appModel.applyFBInfo(this._fbModel.userInfo);
      this._appModel.viewState = Config.State.Screen.SELECTION;
    },

    /**
     * Class identifier
     * @return {string}
     */
    toString: function() {
      return 'LandingScreenController';
    }

  };

  LandingScreenController.$inject = dependencies;
  angular.module('App').controller(
    'LandingScreenController', LandingScreenController
  );

})();
