/**
 * Created with PhpStorm.
 * User: kimh
 * Date: 16/07/14
 * Time: 10:15 AM
 */

 /* global angular, utils, log */

(function() {
  'use strict';

  var $rootScope, $scope, Config, Metrics;
  var dependencies = [
    '$scope', '$rootScope', '$element', 'Config', 'AppModel', 'FBModel',
    'Metrics'
  ];

  function MainController(
    _$scope, _$rootScope, $element, _Config, AppModel, FBModel, _Metrics
    ) {

    // globals:
    Config    = _Config;
    $scope    = _$scope;
    $rootScope = _$rootScope;
    Metrics   = _Metrics;

    // class props:
    this._el  = $element;

    // scope props:
    $scope.permitCopy = Config.permitCopy;
    $scope.name = 'MainScreen';
    $rootScope.API = {};

    // scope refs:
    $scope.fbModel = this._fbModel = FBModel;
    $scope.appModel = this._appModel = AppModel;
    $scope.getCurrentScreen = angular.bind(this,this.getCurrentScreen);

    // wait for content to be loaded before completing initialisation:
    this._unwatchInit = $scope.$on(
      '$includeContentLoaded', angular.bind(this, this.init)
    );
  }

  MainController.prototype = {

    /**
     * Initialise class
     * Executed when first view is loaded.
     */
    init: function(evt) {
      log(this + '::init()');

      // remove init event listener:
      this._unwatchInit();

      this._appModel.abortApp = !this._browserIsOK();
      if(this._appModel.abortApp) {
        $rootScope.$broadcast(Config.Event.ALERT_SHOW, {
          title: Config.Messages.ALERT_BROWSER.title,
          message: Config.Messages.ALERT_BROWSER.message,
          buttonLabel: Config.Messages.ALERT_BROWSER.buttonLabel,
          state: 'button',
          image: 'img/wheelchair.png',
          action: 'upgrade'
        });
      }

      this._addListeners();

      this._fbModel.init(
        Config.LIVE ? Config.ID.live : Config.ID.test
      );

      Metrics.trackPageView(
        Config.State.Screen.LANDING.replace(/\.?html/gi,'')
      );
    },

    /**
     * Check browser meets minimum requirements
     * TEST: has querySelector
     * TEST: >= IE9
     * @private
     */
    _browserIsOK: function() {
      return Element.prototype.hasOwnProperty('querySelector') &&
        utils.gteIE9();
    },

    /**
     * Setup event listeners
     * @private
     */
    _addListeners: function() {
      var _this = this;

      // copy access token info appModel:
      $scope.$watch('fbModel.auth', function(newvalue) {
        if(!newvalue) return;
        _this._appModel.setAuth(newvalue);
      });

      // watch for authentication/fatal errors:
      $scope.$on(Config.Event.APP_ERROR, function(evt, type, isFatal) {
        $rootScope.$broadcast(Config.Event.ALERT_SHOW, {
          title: Config.Messages.ALERT_FATAL.title,
          message: Config.Messages.ALERT_FATAL.message,
          buttonLabel: Config.Messages.ALERT_FATAL.buttonLabel,
          state: 'button',
          action: 'reload'
        });

        Metrics.trackError(type, isFatal);
      });

      // dispatch ga pageviews on navigation:
      $scope.$watch('appModel.viewState', function(nval, oval) {
        if(oval === nval) return;
        Metrics.trackPageView(nval.replace(/\.?html/gi,''));

        // show reveal overlay:
        $scope.API.reveal.show();
      });
    },

    /**
     * return currentScreen
     * @return {string}
     */
    getCurrentScreen: function() {
      return this._appModel.viewState;
    },

    /**
     * Class identifier
     * @return {string}
     */
    toString: function() {
      return 'MainController';
    }
  };

  /**
   * Setup dependencies:
   */
  MainController.$inject = dependencies;
  angular.module('App').controller('MainController', MainController);

})();
