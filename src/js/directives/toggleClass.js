/**
 * Created with WebStorm.
 * User: kimh
 * Date: 26/07/2014
 * Time: 17:32
 */

/* global angular, log */

(function() {

  'use strict';

  angular.module('App').directive('toggleclass', function() {

    return {

      restrict: 'A',
      scope: {
        selected: '=ngModel'
      },
      link: function(scope, element, attrs) {

        var cls = attrs.toggleclass;
        scope.selected = element.hasClass(cls);

        element.on('click', toggle);

        /**
         * Toggle the class
         */
        function toggle() {
          scope.selected = !scope.selected;
          element.toggleClass(cls, scope.selected);
          scope.$apply();
        }
      }

    };

  });

})();
