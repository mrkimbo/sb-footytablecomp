/**
 * Created with WebStorm.
 * User: kimh
 * Date: 29/07/2014
 * Time: 12:22
 */

/* global angular, log, utils, TweenLite, Quad */

(function() {

  'use strict';

  angular.module('App').directive('alert', function($window, Config) {

    return {
      restrict: 'AE',
      scope: {},
      templateUrl: 'html/alert.html',
      replace: true,
      priority: -1000, // low priority so initialises last
      link: function(scope, element) {

        element.toggleClass('ng-hide', true);
        element.css('opacity', 0);

        scope.name = 'Alert';

      },

      controller: function($scope, $element, $sce) {

        var msgBox = angular.element($element[0].querySelector('.msg-box'));
        var anim = null;

        // set up scope properties/event listeners
        $scope.$on(Config.Event.ALERT_SHOW, show);
        $scope.$on(Config.Event.ALERT_HIDE, hide);
        $scope.onButtonClick = onButtonClick;

        /**
         * Reset animation
         */
        function reset() {
          if(anim) {
            anim.kill();
            anim = null;
          }
        }


        /**
         * Show the alert
         * @param {object} evt
         * @param {object} data
         */
        function show(evt, data) {
          log('Alert::show()', data);

          $scope.title = data.title;
          $scope.message = $sce.trustAsHtml(data.message);
          $scope.buttonLabel = data.buttonLabel || 'OK';
          $scope.state = data.state;
          $scope.action = data.action;
          $scope.image = data.image;

          // alert is shown at top of page, so make sure page is
          // scrolled to top:
          $window.scrollTo(0,0);

          msgBox.toggleClass('img', angular.isDefined(data.image));
          $element.toggleClass('ng-hide', false);
          reset();
          anim = TweenLite.to($element[0], 0.2, {
            opacity: 1,
            ease: Quad.easeOut,
            onUpdate: utils.applyCSS,
            onComplete: reset
          });
        }

        /**
         * Hide the alert
         */
        function hide() {
          log('Alert::hide()');

          delete $scope.title;
          delete $scope.message;
          delete $scope.action;
          delete $scope.image;

          reset();
          anim = TweenLite.to($element[0], 0.2, {
            opacity: 0,
            ease: Quad.easeOut,
            onUpdate: utils.applyCSS,
            onComplete: function() {
              $element.toggleClass('ng-hide', true);
              reset();
            }
          });
        }

        function onButtonClick(evt) {

          switch ($scope.action) {
            case 'reload':
              window.location.reload();
              break;

            case 'upgrade':
              window.location = 'http://www.browsehappy.com';
              break;
          }

          hide();
        }

      }

    };

  });

})();
