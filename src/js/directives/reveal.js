/**
 * Created with WebStorm.
 * User: kimh
 * Date: 2/09/2014
 * Time: 9:31
 */

/* global angular, TweenLite, Quad, utils */

(function() {

  'use strict';

  angular.module('App').directive('reveal', function() {

    return {

      restrict  : 'A',
      template  : '<div id="reveal"></div>',
      replace   : true,
      scope     : {
        api: '='
      },

      controller: function($scope, $element) {

        var el = $element[0];
        var anim = null;

        function reset() {
          if(anim) {
            anim.kill();
            anim = null;
          }
          el.removeAttribute('style');
          $element.toggleClass('ng-hide', false);
        }

        function hide() {
          reset();
          anim = TweenLite.to(el, 0.4, {
            opacity     : 0,
            ease        : Quad.easeOut,
            onUpdate    : utils.applyCSS,
            onComplete  : function() {
              el.removeAttribute('style');
              $element.toggleClass('ng-hide', true);
              anim = null;
            }
          });
        }

        // add methods to exposed API:
        $scope.api.reveal = {
          show: reset,
          hide: hide
        };
      }

    };

  });

})();
