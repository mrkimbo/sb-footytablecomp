/**
 * Created with WebStorm.
 * User: kimh
 * Date: 23/07/2014
 * Time: 14:08
 */

/* global angular */

(function() {

  'use strict';

  angular.module('App').directive('resizable', function($window) {

    return function($scope) {

      $scope.onWindowResize = function() {
        $scope.windowHeight = $window.innerHeight;
        $scope.windowWidth = $window.innerWidth;
      };
      $scope.onWindowResize();

      return angular.element($window).bind('resize', function() {
        $scope.onWindowResize();
        return $scope.$apply();
      });
    };

  });

})();
