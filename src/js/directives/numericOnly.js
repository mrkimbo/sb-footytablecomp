/**
 * Created with WebStorm.
 * User: kimh
 * Date: 4/08/2014
 * Time: 14:45
 */

/* global angular, log */

(function() {

  'use strict';

  angular.module('App').directive('numericOnly', function($window) {

    return {

      restrict: 'A',
      scope: {},

      link: function(scope, element) {

        // allow numbers only:
        var regex = /[0-9]/;
        var fnKey = [8,37,39,46]; //arrow keys, backspace, delete
        element.bind('keypress', onKeyPress);

        /**
         * Disallow invalid character input
         * @param evt
         * @return {boolean}
         */
        function onKeyPress(evt) {
          var key = (evt.keyCode||evt.which);
          //log('numericOnly::onKeyPress()', key, evt);

          if(
            fnKey.indexOf(key) !== -1 ||
            regex.test(String.fromCharCode(key))
          ) return;

          // invalid key - disallow:
          evt.preventDefault();
          return false;
        }
      }

    };

  });

})();
