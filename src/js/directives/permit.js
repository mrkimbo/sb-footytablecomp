/**
 * Created with WebStorm.
 * User: kimh
 * Date: 7/08/2014
 * Time: 10:16
 */

/* global angular, log */

(function() {

  'use strict';

  // Reasonably pointless directive, but good to practise....
  angular.module('App').directive('permit', function(Config) {

    return {

      restrict: 'A',
      template: '<div class="permit"><p>{{gamble}}</p><p>{{permit}}</p></div>',
      replace: true,
      scope: {},

      link: function(scope, element) {

        scope.gamble = Config.Messages.GAMBLE;
        scope.permit = Config.Messages.PERMIT;

      }

    };


  });

})();