
/* global angular */

var app;

app = angular.module('App', ['ngTouch', 'taiPlaceholder']).config(
  function($logProvider) {
    'use strict';

    $logProvider.debugEnabled(!!window.DEBUG);
  }
);
