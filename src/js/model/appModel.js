/**
 * Created with WebStorm.
 * User: kimh
 * Date: 17/07/2014
 * Time: 10:42
 */

/* global angular, log */

(function() {

  'use strict';

  var APIService, Config, $rootScope, $timeout, Metrics;
  var dependencies = [
    '$rootScope', '$timeout', 'APIService', 'Config', 'Metrics'
  ];

  var AppModel = function(
    _$rootScope, _$timeout, _APIService, _Config, _Metrics
    ) {

    APIService = _APIService;
    Config = _Config;
    Metrics = _Metrics;
    $rootScope = _$rootScope;
    $timeout = _$timeout;

    // flag whether app can continue:
    this.abortApp = false;

    this.viewState = Config.State.Screen.LANDING;
    this.accessToken = null;
    this.fbPostComplete = false;

    // Form data:
    this.firstName = '';
    this.lastName = '';
    this.email = '';
    this.state = '';
    this.phone = '';

    this.states = [
      { name: 'State', value: ''},
      { name: 'ACT', value: 'ACT'},
      { name: 'NT', value: 'NT'},
      { name: 'NSW', value: 'NSW'},
      { name: 'QLD', value: 'QLD'},
      { name: 'SA', value: 'SA'},
      { name: 'WA', value: 'WA'},
      { name: 'VIC', value: 'VIC'},
      { name: 'TAS', value: 'TAS'}
    ];

    // seat selections:
    this.selections = new Array(10);

    this.init();
  };

  AppModel.prototype = {

    /**
     * Initialisation
     */
    init: function() {
      APIService.apiSuccessHandler = angular.bind(this, this._onAPIResponse);
      APIService.apiErrorHandler = angular.bind(this, this._onAPIError);
    },

    /**
     * Populate access token in APIService
     * @param {string} token
     */
    setAuth: function(auth) {
      log(this + '::setAccessToken()',auth);
      APIService.auth = auth;
    },

    /**
     * Check if all seats on table are filled.
     * @return {boolean}
     */
    tableIsFull: function() {
      for(var i=0, len=this.selections.length; i<len; i++) {
        if(!angular.isDefined(this.selections[i])) return false;
      }
      Metrics.trackEvent(Metrics.Event.TABLE_FILLED, 1);
      return true;
    },

    /**
     * Pre-populate entry form with user's FB details
     * @param {Object} fbInfo
     */
    applyFBInfo: function(fbInfo) {
      log(this + '::applyFBInfo()',fbInfo);
      this.firstName = fbInfo.first_name;
      this.lastName = fbInfo.last_name;
      this.email = fbInfo.email;
    },

    /**
     * Check if we have all required info for a valid entry
     * @return {boolean}
     */
    entryComplete: function() {
      return this.tableIsFull() &&
        this.firstName !== '' &&
        this.lastName !== '' &&
        this.email !== '' &&
        this.state !== '' &&
        this.phone !== '';
    },

    /**
     * Save entry to DB.
     */
    save: function() {

      Metrics.trackEvent(Metrics.Event.ENTRY_SUBMITTED, 1);

      // Create entry data:
      var names = [];
      angular.forEach(this.selections, function(friend) {
        names.push(friend.name);
      });
      var data = {
        first_name: this.firstName,
        last_name: this.lastName,
        email: this.email,
        state: this.state,
        phone: this.phone.replace(/[^0-9]/g, ''),
        selections: names.join(',')
      };

      // Show user alert to confirm submission initiated:
      $rootScope.$broadcast(Config.Event.ALERT_SHOW, {
        title: Config.Messages.ALERT_SUBMIT.title,
        message: Config.Messages.ALERT_SUBMIT.message,
        state: 'loader'
      });

      log(this + '::save()', data);

      APIService.save(data);
    },

    /**
     * Post to users' FB timeline.
     */
    postToFacebook: function() {

      Metrics.trackEvent(Metrics.Event.SUBMIT_POST_CLICKED, 1);

      // Create entry data:
      var images = [], tags = [];
      angular.forEach(this.selections, function(friend) {
        images.push(friend.img);
        if(friend.id) tags.push(friend.id);
      });
      var data = {
        avatars: images.join(','),
        tags: tags.join(',')
      };

      // Show user alert to confirm post submission initiated:
      $rootScope.$broadcast(Config.Event.ALERT_SHOW, {
        title: Config.Messages.ALERT_FB_POST.title,
        message: Config.Messages.ALERT_FB_POST.message,
        state: 'loader'
      });

      log(this + '::postToFacebook()', data);

      APIService.submitPost(data);
    },

    /**
     * Select a friend in the friend list.
     * @param {Object} friend Friend object
     * @param {number=} opt_idx Optional seat number
     */
    selectFriend: function(friend, opt_idx) {
      this.selections[opt_idx || this._getFirstEmptySeat()] = {
        name: friend.name,
        id: friend.id,
        img: friend.picture.data.url
      };
    },

    /**
     * Unselect a friend in the friend list.
     * @param {Object} friend Friend object
     * @param {number=} opt_idx Optional seat number
     * @return {boolean}
     */
    deselectFriend: function(friend, opt_idx) {
      for(var i=0; i<this.selections.length; i++) {
        if(!angular.isDefined(this.selections[i])) continue;
        if(this.selections[i].id === friend.id) {
          delete this.selections[i];
          return true;
        }
      }
      return false;
    },

    /**
     * Check server fb authentication status:
     */
    verifySession: function() {
      log(this + '::verifySession()');
      APIService.verifySession();
    },

    /**
     * Find first available seat at the table.
     * @return {number}
     * @private
     */
    _getFirstEmptySeat: function() {
      var i = -1;
      while(++i<this.selections.length) {
        if(!angular.isDefined(this.selections[i])) return i;
      }
      return -1;
    },

    /**
     * Handle successful API response
     * @param {string} requestType
     * @param {Object} response
     * @private
     */
    _onAPIResponse: function(requestType, response) {
      log(this + '::onAPIResponse(' + requestType + ')',response);

      // handle failed process:
      if(!response.success) {
        this._onAPIError.apply(this, arguments);
        return;
      }

      switch(requestType) {
        case 'save':
          // entry saved - hide alert and navigate to confirmation screen:
          $rootScope.$broadcast(Config.Event.ALERT_HIDE);
          this.viewState = Config.State.Screen.CONFIRMATION;
          break;

        case 'post':
          $rootScope.$broadcast(Config.Event.ALERT_HIDE);
          this.fbPostComplete = true;
          Metrics.trackEvent(Metrics.Event.POST_PUBLISHED);
          break;
      }
    },

    /**
     * Handle API error
     * @param {string} responseType
     * @param {Object} response
     * @private
     */
    _onAPIError: function(responseType, response) {
      log(this + '::onAPIError()',arguments);

      // All API errors are considered fatal and need a page reload:
      $rootScope.$broadcast(Config.Event.APP_ERROR);
    },

    /**
     * Class identifier
     * @return {string}
     */
    toString: function() {
      return 'AppModel';
    }

  };

  AppModel.$inject = dependencies;
  angular.module('App').service('AppModel', AppModel);

})();
