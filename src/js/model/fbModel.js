/**
 * Created with WebStorm.
 * User: kimh
 * Date: 12/07/2014
 * Time: 16:11
 */

/* global angular, FB, log, utils */

(function() {

  'use strict';

  var $rootScope, FBService, APIService, Config, Metrics;
  var basicPermissions    = 'public_profile,email,user_friends';
  var extendedPermissions = 'publish_actions';
  var dependencies = [
    '$rootScope', 'FacebookService', 'APIService', 'Config', 'Metrics'
  ];

  var FBModel = function(
    _$rootScope, _FacebookService, _APIService, _Config, _Metrics
    ) {

    // globals:
    $rootScope = _$rootScope;
    FBService = _FacebookService;
    APIService = _APIService;
    Config = _Config;
    Metrics = _Metrics;

    this.state = Config.State.FB.NOT_SET;
    this.appId = null;
    this.auth = null;

    this.userInfo = null;
    this.friends = null;
    this.canPost = null;

    if(!Config.LIVE) {
      this.state = Config.State.FB.APP_AUTH;
      this.userInfo = Config.getDummyProfile();
      this.friends = Config.getDummyFriends(500);
    }
  };

  FBModel.prototype = {

    /**
     * Initialisation
     * @param appId
     */
    init: function(appId) {
      log(this + '::init(' + appId + ')');

      this.appId = appId;
      this._addFBListeners();

      FBService.statusCallback = angular.bind(this, this._onStatusChange);
      FBService.apiSuccessHandler = angular.bind(this, this._onAPIResponse);

      FBService.init(this.appId);
    },

    /**
     * Add watchers and event listeners
     * @private
     */
    _addFBListeners: function() {

      // Handle FB logout events:
      FB.Event.subscribe('auth.logout', function(response) {
        $rootScope.$broadcast(Config.Event.APP_ERROR);
      });

      // Handle FB status change events:
      FB.Event.subscribe(
        'auth.statusChange', angular.bind(this, this._onStatusChange)
      );

      // Handle Sportsbet page like events:
      FB.Event.subscribe('edge.create', angular.bind(this, this._onPageLike));
    },

    /**
     * Handle page like event
     * @param {string} href
     * @private
     */
    _onPageLike: function(href) {
      if(href.indexOf('sportsbetcomau') === -1) return;
      log(this + '::FB:SportsbetPageLiked()');

      Metrics.trackEvent(Metrics.Event.FB_PAGE_LIKED, 1);
    },

    /**
     * Handle facebook status update
     * @param {object} response
     * @private
     */
    _onStatusChange: function(response) {
      log(this + '::onStatusChange()', response);

      var status = (response.status || '').toLowerCase();
      switch(status) {
        case 'connected':
          // if already authorised, then this is a ext permissions request
          // callback
          if(this.isAuthorised()) {
            FBService.checkPermissions();
            return;
          }

          this.auth = response.authResponse;
          this.state = Config.State.FB.APP_AUTH;

          // resave cookie:
          updateFBCookie(this.accessToken, this.appId);

          FBService.getBasicInfo();
          break;

        case 'not_authorized':
          this.state = Config.State.FB.FB_AUTH;
          break;

        default:
          this.state = Config.State.FB.UNKNOWN;
          break;
      }

      $rootScope.$broadcast(Config.Event.FB_MODEL_UPDATED, 'status');
    },

    /**
     * Handle response from APIService
     * @param {string} requestType
     * @param {object} response
     * @private
     */
    _onAPIResponse: function(requestType, response) {
      log(this + '::onAPIResponse(' + requestType + ')', response);

      if(!response || angular.isDefined(response.error)) {
        this._onAPIError.apply(this, arguments);
        return;
      }

      switch(requestType) {
        case 'basic-info':
          this.userInfo = response;
          break;

        case 'profile-img':
          this.userInfo.profileImg = response.data.url;
          break;

        case 'friends':
          this.friends = response.data;
          if(this.friends.length === 0) {
            $rootScope.$broadcast(Config.Event.APP_ERROR);
            return;
          }
          // sort alphabetically:
          this.friends.sort(function(a, b) {
            return a.name < b.name ? -1 : 1;
          });
          break;

        case 'perms':
          this.canPost = false;
          angular.forEach(response.data, function(value) {
            if(value.permission === 'publish_actions') {
              this.canPost = value.status === 'granted';
            }
          }, this);

          // capture event if user has accepted post perms when submitting:
          if(this.canPost) {
            Metrics.trackEvent(Metrics.Event.FB_CAN_PUBLISH, 1);
          }
          break;
      }

      $rootScope.$broadcast(Config.Event.FB_MODEL_UPDATED, requestType);
    },

    /**
     * Handle error from APIService
     * @param {string} responseType
     * @param {object} response
     * @private
     */
    _onAPIError: function(responseType, response) {
      log(this + '::onAPIError()',arguments);
    },

    /**
     * Check if we has full authentication for the app
     * @return {boolean}
     */
    isAuthorised: function() {
      return this.state === Config.State.FB.APP_AUTH;
    },

    /**
     * Request user information from the facebook api
     */
    fetchUserInfo: function() {
      FBService.getProfileImage();
      FBService.getTaggableFriends();
      FBService.checkPermissions();
    },

    /**
     * Request a login for facebook.
     */
    login: function() {
      // facebook doesn't allow OAUTH in Chrome on iOS:
      if(utils.isChromeIOS()){
        window.location.replace(
          'https://www.facebook.com/dialog/oauth?' +
            'client_id=' + this.appId + '&' +
            'redirect_uri=' + window.location.href + '&' +
            'scope=' + extendedPermissions
        );
      } else {
        FBService.login(basicPermissions);
      }
    },

    /**
     * Request extra permissions (publish)
     */
    requestPublishPerms: function() {
      FBService.login(extendedPermissions);
    },

    /**
     * Class identifier
     * @return {string}
     */
    toString: function() {
      return 'FBModel';
    }
  };


  /**
   * Update Facebook Signed Request Toke in cookie in case expired.
   * @param {string} token
   * @param {string} app_id
   * @return {boolean} process success
   */
  function updateFBCookie(token, app_id) {
    var cookie, items = document.cookie.split(/;[\s]+?/);
    for(var i=0; i<items.length; i++) {
      if(items[i].indexOf('fbsr_' + app_id) === 0) {
        cookie = items[i].split('=');
        return items[i] = cookie[0] + '=' + token, true;
      }
    }
    return false;
  }

  FBModel.$inject = dependencies;
  angular.module('App').service('FBModel', FBModel);

})();
