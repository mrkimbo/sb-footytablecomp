<?
require "api/lib/config.php";
require "api/lib/validation.php";

// check competition is still running:
if(competitionIsClosed()) {
  header('Location: /closed.html');
  exit();
}

?><!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <title>FootyTable Microsite</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <meta property="fb:app_id" content="604289759690594" />
  <meta property="og:title" content="Win Sportsbet’s table at the Grand Final Footy Show!" />
  <meta property="og:description" content="You and your wolfpack could win a table at the Grand Final Footy Show." />
  <meta property="og:url" content="https://footyshowtable.com/" />
  <meta property="og:type" content="article" />
  <meta property="og:image" content="http://i.sbstatic.com.au/marketing/images/banners/2014/socialmedia/fb-open-graph-tag-footyshow-11927.jpg" />
  <script type="text/javascript" src="https://connect.facebook.net/en_US/sdk.js"></script>
  <!--~ AngularJS included in head so it can manage element visibility during page-load/initial setup -->
  <!--+<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.20/angular.min.js"></script>-->
  <script data-remove src="js/third-party/angular.js"></script>
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,900' rel='stylesheet' type='text/css' />
  <!-- build:css css/main.min.css -->
  <link rel="stylesheet" type="text/css" href="css/main.css" />
  <!-- endbuild -->
</head>
<body ng-app="App" ng-cloak>

  <!--
    Start of DoubleClick Floodlight Tag: Please do not remove
    Activity name of this tag: Sportsbet_AFLfootyshowComp
    URL of the webpage where the tag is expected to be placed: http://www.sportsbet.com.au
    This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
    Creation Date: 08/05/2014
  -->
  <script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<iframe src="http://3674310.fls.doubleclick.net/activityi;src=3674310;type=count123;cat=Sport00S;u1=[Account ID];u11=[Platform];ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
  </script>
  <noscript>
    <iframe src="http://3674310.fls.doubleclick.net/activityi;src=3674310;type=count123;cat=Sport00S;u1=[Account ID];u11=[Platform];ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
  </noscript>
  <!-- End of DoubleClick Floodlight Tag: Please do not remove -->

  <!--~ google analytics -->
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-2800569-33', 'auto');
    ga('send', 'pageview');
  </script>


  <!--~ CONTENT -->
  <div id="main" ng-controller="MainController">
    <div resizable id="content" ng-include="getCurrentScreen()"></div>
  </div>

  <div reveal api="API"></div>
  <div alert></div>

  <!--~ CDN Scripts -->
  <!--+<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.10.3/TweenLite.min.js"></script>-->
  <!--+<script src="http://cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.20/angular-touch.min.js"></script>-->

  <!--~ temp local scripts -->
  <script data-remove src="js/third-party/angular-touch.js"></script>
  <script data-remove src="js/third-party/TweenLite.min.js"></script>
  <script data-remove src="../node_modules/faker/faker.js"></script>

  <!-- build:js({.tmp,src}) js/main.min.js -->
  <script src='js/third-party/tai-placeholder.js'></script>
  <script src="js/utils.js"></script>
  <script src="js/app.js"></script>
  <script src="js/controllers/main.js"></script>
  <script src="js/controllers/landingScreen.js"></script>
  <script src="js/controllers/selectionScreen.js"></script>
  <script src="js/controllers/entryForm.js"></script>
  <script src="js/controllers/confirmationScreen.js"></script>
  <script src="js/model/appModel.js"></script>
  <script src="js/model/fbModel.js"></script>
  <script src="js/providers/facebookService.js"></script>
  <script src="js/providers/apiService.js"></script>
  <script src="js/providers/config.js"></script>
  <script src="js/providers/metrics.js"></script>
  <script src="js/directives/permit.js"></script>
  <script src="js/directives/alert.js"></script>
  <script src="js/directives/reveal.js"></script>
  <script src="js/directives/numericOnly.js"></script>
  <script src="js/directives/resizable.js"></script>
  <script src="js/directives/toggleclass.js"></script>
  <!-- endbuild -->

</body>
</html>