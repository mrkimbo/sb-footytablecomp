<!DOCTYPE html>
<html xmlns:fb="http://ogp.me/ns/fb#">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <title>FootyTable Microsite - Coming soon</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
  <meta property="fb:app_id" content="604289759690594" />
  <meta property="og:title" content="Win Sportsbet’s table at the Grand Final Footy Show!" />
  <meta property="og:description" content="You and your wolfpack could win a table at the Grand Final Footy Show." />
  <meta property="og:url" content="https://footyshowtable.com/" />
  <meta property="og:type" content="article" />
  <meta property="og:image" content="http://i.sbstatic.com.au/marketing/images/banners/2014/socialmedia/fb-open-graph-tag-footyshow-11927.jpg" />
</head>
<body>You should not be looking at this. Close your eyes immediately!</body>
</html>